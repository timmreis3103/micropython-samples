import sys 
sys.path.append('flowlib/lib')
import m5base
from m5stack import lcd
import gc
import os
import time

__VERSION__ = m5base.get_version()

lcd.image(0,0,"img/m5.jpg")
time.sleep_ms(1500)
lcd.clear()
lcd.image(lcd.CENTER, lcd.CENTER, 'img/uiflow_logo.bmp')
lcd.setColor(0xCCCCCC, 0)
lcd.print(__VERSION__, 260, 5)
lcd.print("MicroPython " + os.uname().version, 0, 180)
gc.collect()
lcd.print("Free RAM = " + str(gc.mem_free()) + ' bytes', 0, 225)
lcd.setCursor(0, 0)
time.sleep_ms(1500)
