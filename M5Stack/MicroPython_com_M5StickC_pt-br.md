# MicroPython com M5StickC

#### Autor : Roberto Colistete Jr. (roberto.colistete arroba gmail.com)
##### Última atualização : 24/03/2020.

<img src="https://docs.m5stack.com/assets/img/product_pics/core/minicore/m5stickc/m5stickc_01.jpg" alt="M5StickC" width="300"/>

M5StickC, US$9.99 (com cabo USB-C pequeno e caixinha), modelos de 07/2019 a 01/2020 :  
[M5StickC Docs](https://docs.m5stack.com/#/en/core/m5stickc)  
[M5StickC @ M5Stack Store](https://m5stack.com/collections/m5-core/products/stick-c)  
[M5StickC @ M5Stack AliExpress Store](https://www.aliexpress.com/item/32985247364.html)  
ESP32 520KB SRAM, 4MB flash, TFT Colour LCD 0.96" 80x160 ST7735S, MPU6886 acell/gyro, microphone SPM1423, RTC BM8563, 2 user buttons, 1 power/reset button,
80 mAh battery, red LED, IR transmitter, USB-C (power/serial), Grove connector, 8 female headers, orange case, 48.2 mm x 25.5 mm x 13.7 mm, 15.1 g.


Ligar pressionando o botão lateral esquerdo.  
Pressionar por 6s o botão lateral esquerdo para desligar.  

Nos códigos-fonte de testes :
- digite tecla <Tab> após '.' em final de linha para ver a lista de atributos do objeto;
- saídas/resultados  estão indentados com 1 tab a mais.



## Opções de firmware MicroPython para M5StickC :

1. [UIFlow 1.4.5.1](#1-uiflow-1451-) é baseado em MicroPython 1.11 (29/05/2019) oficial para ESP32, com grande personalização da M5Stack, incluindo partes de MicroPython 
ESP32 psRAM LoBo. Reconhece a tela e quase todo o hardware, exceto transmissor IR e microfone. Tem vários drivers para as 'Units' e 'C-Hats'. 
Mas : os drivers na maioria têm recursos simplificados; o código-fonte é fechado; a documentação de meados de 2019 para cá é incompleta;

2. [MicroPython oficial v1.12 (12/2019) ou mais recente](#2-micropython-oficial-v112-ou-mais-recente), com código-fonte aberto, mais RAM livre e menor consumo de energia. Mas tem que instalar 
manualmente drivers de terceiros para a tela ST7735S, acelerômetro & giroscópio MPU6886, RTC BM8563 e CI gerenciador de energia AXP192, bem como para 
para os vários dispositivos em 'Units' e 'C Hats'.



## 1. UIFlow 1.4.5.1 :

UIFlow >= 1.3.2 é baseado em Micropython oficial, no caso UIFlow 1.4.5.1 usa 
[Micropython v1.11 oficial de 29/05/2019](https://github.com/micropython/micropython/releases/tag/v1.11) com extensas personalizações da M5Stack, 
incluindo partes (ADC, DAC, I2C, etc) de [MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/dac).  
Reconhece a tela ST7735S, acelerômetro & giroscópio MPU6886, RTC BM8563 e CI gerenciador de energia AXP192. Mas não tem driver/módulo para microphone 
SPM1423 e transmissor IR. Tem vários drivers para as 'Units' e 'C-Hats'. Mas :  
- os drivers na maioria têm recursos simplificados; 
- o código-fonte é fechado, sendo uma mistura não desconhecida de 2 MicroPython para ESP32 : oficial e Lobo;
- a documentação das personalizações da M5Stack é de meados de 2019 para cá e incompleta;


### 1.1 Instalando firmware :

Via terminal, apagando flash via ['esptool'](https://pypi.org/project/esptool/) :  
`$ pip install esptool`  
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
e instalando firmware UIFlow, dentro de 'UIFlow-Desktop-IDE_Linux_v1.4.5.1/assets/firmwares/UIFlow-v1.4.5.1/UIFlow-Firmware-1.4.5.1/firmware_StickC/' :  
`$ ./flash`


### 1.2 Testes e exemplos :

- tem muitos arquivos (imagens na maioria) em '/flash', que podem ser copiados usando ['rshell'](https://github.com/dhylands/rshell) (mais fácil) 
ou ['ampy'](https://github.com/scientifichackers/ampy) :
```
$ rshell -p /dev/ttyUSB0
> ls -l /flash
         0 Feb 24 2050  apps/
         0 Feb 24 2050  blocks/
        1818 Feb 24 2050  boot.py
        147 Dec 31 2009  config.json
        49 Feb 24 2050  config.py
        275 Feb 24 2050  debug.py
        866 Feb 24 2050  flow.py
        650 Feb 24 2050  flow_usb.py
         0 Feb 24 2050  image_app/
         0 Feb 24 2050  img/
        55 Feb 24 2050  main.py
         0 Feb 24 2050  res/
```

- dando reset apertando uma vez o botão laterial esquerdo :
```
I (8) boot: ESP-IDF v3.3-beta1-696-gc4c54ce07 2nd stage bootloader
I (9) boot: compile time 19:37:28
I (9) boot: Enabling RNG early entropy source...
I (13) boot: SPI Speed      : 80MHz
I (17) boot: SPI Mode       : DIO
I (21) boot: SPI Flash Size : 4MB
I (26) boot: Partition Table:
I (29) boot: ## Label            Usage          Type ST Offset   Length
I (36) boot:  0 nvs              WiFi data        01 02 00009000 00006000
I (44) boot:  1 phy_init         RF data          01 01 0000f000 00001000
I (51) boot:  2 factory          factory app      00 00 00010000 00200000
I (59) boot:  3 internalfs       Unknown data     01 81 00210000 001ef000
I (66) boot: End of partition table
I (70) esp_image: segment 0: paddr=0x00010020 vaddr=0x3f400020 size=0xea84c (960588) map
I (355) esp_image: segment 1: paddr=0x000fa874 vaddr=0x3ffbdb60 size=0x034dc ( 13532) load
I (360) esp_image: segment 2: paddr=0x000fdd58 vaddr=0x40080000 size=0x00400 (  1024) load
I (362) esp_image: segment 3: paddr=0x000fe160 vaddr=0x40080400 size=0x01eb0 (  7856) load
I (374) esp_image: segment 4: paddr=0x00100018 vaddr=0x400d0018 size=0xdcac4 (903876) map
I (639) esp_image: segment 5: paddr=0x001dcae4 vaddr=0x400822b0 size=0x13d38 ( 81208) load
I (667) esp_image: segment 6: paddr=0x001f0824 vaddr=0x400c0000 size=0x00064 (   100) load
I (667) esp_image: segment 7: paddr=0x001f0890 vaddr=0x50000000 size=0x00808 (  2056) load
I (687) boot: Loaded app from partition at offset 0x10000
I (688) boot: Disabling RNG early entropy source...
I (688) cpu_start: Pro cpu up.
I (692) cpu_start: Application information:
I (696) cpu_start: Compile time:     Feb 25 2020 19:37:33
I (703) cpu_start: ELF file SHA256:  0000000000000000...
I (708) cpu_start: ESP-IDF:          v3.3-beta1-696-gc4c54ce07
I (715) cpu_start: Starting app cpu, entry point is 0x400836e4
I (717) cpu_start: App cpu up.
I (726) heap_init: Initializing. RAM available for dynamic allocation:
I (732) heap_init: At 3FFAE6E0 len 0000F480 (61 KiB): DRAM
I (739) heap_init: At 3FFCB2D8 len 00014D28 (83 KiB): DRAM
I (745) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (751) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (758) heap_init: At 40095FE8 len 0000A018 (40 KiB): IRAM
I (764) cpu_start: Pro cpu start user code
I (110) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.

Internal FS (FatFS): Mounted on partition 'internalfs' [size: 2027520; Flash address: 0x210000]
----------------
Filesystem size: 1974272 B
           Used: 479232 B
           Free: 1495040 B
----------------
I (484) [TFTSPI]: attached display device, speed=8000000
I (484) [TFTSPI]: bus uses native pins: false
[ M5 ] node id:d8a01d515e2c, api key:C31B6315
I (4767) system_api: Base MAC address is not set, read default base MAC address from BLK0 of EFUSE
I (4768) system_api: Base MAC address is not set, read default base MAC address from BLK0 of EFUSE
I (4882) phy: phy_version: 4100, 2a5dd04, Jan 23 2019, 21:00:07, 0, 0
I (4889) modsocket: Initializing
WiFi AP WebServer Start!
Connect to Wifi SSID:M5-5e2c
And connect to esp via your web browser (like 192.168.4.1)
listening on ('0.0.0.0', 80)
```

- com 'boot.py' e 'main.py' de 'UIFlow_v1.45.1 - Base Micropython v1.11' :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.11-321-gac7da0a70-dirty on 2020-02-25; ESP32 module with ESP32  
import gc
gc.collect()
gc.mem_free()
    66832   # 65.3 kB
```

- apagando 'main.py' e usando 'boot.py' simplificado :
```
import sys 
sys.path.append('flowlib/lib')
import m5base
from m5stack import lcd
import gc
import os
import time

__VERSION__ = m5base.get_version()

lcd.clear(lcd.BLACK)
lcd.image(lcd.CENTER, lcd.CENTER, "image_app/M5GO.JPG")
time.sleep_ms(1000)
lcd.clear(lcd.BLACK)
lcd.image(lcd.CENTER, 17, 'img/uiflow_logo_80x80.bmp')
lcd.setColor(0xCCCCCC, 0)
lcd.print(__VERSION__, lcd.CENTER, 10)
lcd.font(lcd.FONT_DefaultSmall)
lcd.print("MicroPython", lcd.CENTER, 100)
lcd.print('v' + os.uname().release, lcd.CENTER, 111)
lcd.print(os.uname().version[-10:], lcd.CENTER, 122)
gc.collect()
lcd.print("Free RAM", lcd.CENTER, 139)
lcd.print('{:.2f}'.format(gc.mem_free()/1024) + ' kB', lcd.CENTER, 150)
time.sleep_ms(1000)
```
O mesmo será usado nos exemplos e testes seguintes :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.11-312-g944acf81d-dirty on 2020-02-25; ESP32 module with ESP32
import gc
gc.collect()
gc.mem_free()  
    78160   # 76.3 kB
```

- com 'boot.py' mínimo :
```
import sys 
sys.path.append('flowlib/lib')
```
sem inicializar gráficos, mas REPL fica difícil de ser acessado :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.11-312-g944acf81d-dirty on 2020-02-25; ESP32 module with ESP32  
import gc
gc.collect()
gc.mem_free()
    85424   # 83.4 kB
```

- ver versão de MicroPython (após boot/reset também mostra) :
```
import os
os.uname()
    (sysname='esp32', nodename='esp32', release='1.11.0', version='v1.11-321-gac7da0a70-dirty on 2020-02-25', machine='ESP32 module with ESP32')
```

- [módulo 'machine'](https://docs.micropython.org/en/latest/library/machine.html) e frequência do ESP32 (240 MHz, não dá para alterar) :
```
import machine
machine.freq()
    240000000
```

- help :
```
help()
    Welcome to MicroPython on the ESP32!
    
    For generic online docs please visit http://docs.micropython.org/
    
    For access to the hardware use the 'machine' module:
    
    import machine
    pin12 = machine.Pin(12, machine.Pin.OUT)
    pin12.value(1)
    pin13 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
    print(pin13.value())
    i2c = machine.I2C(scl=machine.Pin(21), sda=machine.Pin(22))
    i2c.scan()
    i2c.writeto(addr, b'1234')
    i2c.readfrom(addr, 4)
    
    Basic WiFi configuration:
    
    import network
    sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
    sta_if.scan()                             # Scan for available access points
    sta_if.connect("<AP_name>", "<password>") # Connect to an AP
    sta_if.isconnected()                      # Check for successful connection
    
    Control commands:
      CTRL-A        -- on a blank line, enter raw REPL mode
      CTRL-B        -- on a blank line, enter normal REPL mode
      CTRL-C        -- interrupt a running program
      CTRL-D        -- on a blank line, do a soft reset of the board
      CTRL-E        -- on a blank line, enter paste mode
    
    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do UIFlow MicroPython firmware) :

```
help('modules')
    __main__          flowlib/hats/_servos                flowlib/units/_accel                math
    _onewire          flowlib/hats/_speaker               flowlib/units/_adc                  microWebSocket
    _thread           flowlib/hats/_tof flowlib/units/_angle                microWebSrv
    array             flowlib/hats/_yun flowlib/units/_button               microWebTemplate
    binascii          flowlib/hw/_led   flowlib/units/_cardKB               micropython
    btree             flowlib/hw/axp192 flowlib/units/_color                mlx90640
    builtins          flowlib/hw/bm8563 flowlib/units/_dac                  neopixel
    cmath             flowlib/hw/mpu6050                  flowlib/units/_dual_button          network
    collections       flowlib/hw/sh200q flowlib/units/_earth                os
    display           flowlib/i2c_bus   flowlib/units/_env                  random
    errno             flowlib/lib/bmm150                  flowlib/units/_ext_io               re
    esp               flowlib/lib/bmp280                  flowlib/units/_finger               select
    esp32             flowlib/lib/chunk flowlib/units/_gps                  socket
    espnow            flowlib/lib/dht12 flowlib/units/_ir ssl
    flowlib/app_manage                  flowlib/lib/easyIO                  flowlib/units/_joystick             struct
    flowlib/button    flowlib/lib/imu   flowlib/units/_light                sys
    flowlib/face      flowlib/lib/mstate                  flowlib/units/_makey                time
    flowlib/faces/_calc                 flowlib/lib/numbers                 flowlib/units/_mlx90640             ubinascii
    flowlib/faces/_encode               flowlib/lib/pid   flowlib/units/_ncir                 ucollections
    flowlib/faces/_finger               flowlib/lib/sh200q                  flowlib/units/_pahub                ucryptolib
    flowlib/faces/_gameboy              flowlib/lib/speak flowlib/units/_pbhub                uctypes
    flowlib/faces/_joystick             flowlib/lib/time_ex                 flowlib/units/_pir                  uerrno
    flowlib/faces/_keyboard             flowlib/lib/urequests               flowlib/units/_relay                uhashlib
    flowlib/faces/_rfid                 flowlib/lib/wave  flowlib/units/_rfid                 uhashlib
    flowlib/flowSetup flowlib/lib/wavplayer               flowlib/units/_rgb                  uheapq
    flowlib/hat       flowlib/lib/wifiCardKB              flowlib/units/_rgb_multi            uio
    flowlib/hats/_adc flowlib/lib/wifiCfg                 flowlib/units/_servo                ujson
    flowlib/hats/_beetlec               flowlib/lib/wifiWebCfg              flowlib/units/_tof                  uos
    flowlib/hats/_bugc                  flowlib/m5cloud   flowlib/units/_tracker              upip
    flowlib/hats/_cardKB                flowlib/m5mqtt    flowlib/units/_weight               upip_utarfile
    flowlib/hats/_dac flowlib/m5stack   flowlib/utils     urandom
    flowlib/hats/_env flowlib/m5ucloud  gc                ure
    flowlib/hats/_finger                flowlib/modules/_cellular           hashlib           uselect
    flowlib/hats/_joyC                  flowlib/modules/_pm25               heapq             usocket
    flowlib/hats/_joystick              flowlib/peripheral                  io                ussl
    flowlib/hats/_mlx90640              flowlib/power     json              ustruct
    flowlib/hats/_ncir                  flowlib/remote    lidar             utime
    flowlib/hats/_pir flowlib/simple    logging           utimeq
    flowlib/hats/_powerc                flowlib/statechoose                 m5base            uwebsocket
    flowlib/hats/_puppy                 flowlib/timeSchedule                m5uart            uzlib
    flowlib/hats/_roverc                flowlib/uiflow    m5ui              vl53l0x
    flowlib/hats/_servo                 flowlib/unit      machine           zlib
    Plus any modules on the filesystem
```

- [módulo 'esp'](https://docs.micropython.org/en/latest/library/esp.html) e tamanho da memória flash interna (4 MB) :
```
import esp
esp.
    __class__       __name__        LOG_DEBUG       LOG_ERROR
    LOG_INFO        LOG_NONE        LOG_VERBOSE     LOG_WARNING
    dht_readinto    flash_erase     flash_read      flash_size
    flash_user_start                flash_write     gpio_matrix_in
    gpio_matrix_out                 neopixel_write  osdebug
esp.flash_size()
    4194304   # 4MB
```

- [módulo 'esp32'](https://docs.micropython.org/en/latest/library/esp32.html), temperatura e leitura do sensor Hall do ESP32 :
```
import esp32
esp32.
    __class__       __name__        ULP             WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 hall_sensor     raw_temperature
    wake_on_ext0    wake_on_ext1    wake_on_touch
esp32.raw_temperature()   # in F
    123
(esp32.raw_temperature() - 32)/1.8   # in C
    50.55556
esp32.hall_sensor()
    9
```

- módulo 'm5stack' :
```
import m5stack
    I (213750) [TFTSPI]: attached display device, speed=8000000
    I (213750) [TFTSPI]: bus uses native pins: false
    [ M5 ] node id:d8a01d515e2c, api key:C31B6315
m5stack.
    __class__       __name__        const           __file__
    binascii        display         m5base          machine
    os              time            __VERSION__     apikey
    node_id         lcd             BtnChild        Btn
    btn             timEx           timerSch        btnA
    btnB            M5Led           axp192          axp
    bm8563          rtc             _led            time_ex
    timeSchedule    reboot
```

- [módulo 'm5ui'](https://github.com/m5stack/UIFlow-Code/wiki/M5UI) :
```
import m5ui
m5ui.
    __class__       M5Button        M5Circle        M5Img
    M5Rect          M5TextBox       M5Title         M5UI_Deinit
    setScreenColor
```

- módulo 'uiflow' :
```
import uiflow
uiflow.
    __class__       __name__        const           start
    __file__        binascii        display         gc
    m5base          machine         os              time
    wait            apikey          node_id         flowDeinit
    loopSetIdle     flowExit        lcd             loopExit
    BtnChild        Btn             btn             wait_ms
    timEx           config_normal   cfgWrite        cfgRead
    modeSet         core_start      resetDefault    startBeep
    _p2pData        _nextP2PTime    sendP2PData     setP2PData
    getP2PData      _exitState      loopState       _is_remote
    remoteInit      timerSch        btnA            btnB
    M5Led           axp192          axp             bm8563
    rtc             time_ex         timeSchedule    reboot
```

- [módulo 'm5stack"](https://github.com/m5stack/UIFlow-Code/wiki/Graphic) com gráficos exibindo texto, linhas e imagem na tela TFT de 160x80 pixels :
```
import gc, os
from m5stack import lcd
    I (18378) [TFTSPI]: attached display device, speed=8000000
    I (18378) [TFTSPI]: bus uses native pins: false
    [ M5 ] node id:d8a01d515e2c, api key:C31B6315
lcd.
    __class__       clear           print           BLACK
    BLUE            BMP             BOTTOM          CENTER
    COLOR_BITS16    COLOR_BITS24    CYAN            DARKCYAN
    DARKGREEN       DARKGREY        FONT_7seg       FONT_Comic
    FONT_Default    FONT_DefaultSmall               FONT_DejaVu18
    FONT_DejaVu24   FONT_DejaVu40   FONT_DejaVu56   FONT_DejaVu72
    FONT_Minya      FONT_Small      FONT_Tooney     FONT_Ubuntu
    GREEN           GREENYELLOW     HSPI            JPG
    LANDSCAPE       LANDSCAPE_FLIP  LASTX           LASTY
    LIGHTGREY       M5STACK         M5STICK         MAGENTA
    MAROON          NAVY            OLIVE           ORANGE
    PINK            PORTRAIT        PORTRAIT_FLIP   PURPLE
    RED             RIGHT           VSPI            WHITE
    YELLOW          arc             attrib7seg      backlight
    circle          clearwin        compileFont     deinit
    drawCircle      drawLine        drawPixel       drawRect
    drawRoundRect   drawTriangle    ellipse         fill
    fillCircle      fillRect        fillRoundRect   fillScreen
    fillTriangle    font            fontSize        getCursor
    get_bg          get_fg          hsb2rgb         image
    init            line            lineByAngle     orient
    pixel           polygon         println         qrcode
    rect            resetwin        restorewin      roundrect
    savewin         screensize      setBrightness   setColor
    setCursor       setRotation     setTextColor    set_bg
    set_fg          setwin          text            textClear
    textWidth       text_x          text_y          tft_deselect
    tft_readcmd     tft_select      tft_setspeed    tft_writecmd
    tft_writecmddata                triangle        winsize
lcd.clear(0x000040)   # blue
lcd.font(lcd.FONT_DefaultSmall, color=0xFFFFFF)
lcd.print("MicroPython " + os.uname().version)
gc.collect()
lcd.print("\nFree RAM = " + str(gc.mem_free()) + ' bytes')
#
lcd.clearwin()
for y in range(0, 162, 3):
    lcd.line(0, y, 79, 159, 0xFF0000)
for y in range(0, 162, 3):
    lcd.line(0, 0, 79, y, 0x0000FF)
#
lcd.clearwin()
lcd.image(lcd.CENTER, lcd.CENTER, "image_app/M5GO.JPG")
```

- [módulo 'display'](https://github.com/m5stack/UIFlow-Code/wiki/Display) com funções de mais baixo nível, usando 'boot.py' 
sem inicializar tela (i.e., sem `from m5stack import lcd`).  
[Vide pinagem SPI da tela ST7735](https://blog.csdn.net/zhufu86/article/details/93765269).
```
import display
tft = display.TFT()
tft.
    BLUE            BMP             BOTTOM          CENTER
    COLOR_BITS16    COLOR_BITS24    CYAN            DARKCYAN
    DARKGREEN       DARKGREY        FONT_7seg       FONT_Comic
    FONT_Default    FONT_DefaultSmall               FONT_DejaVu18
    FONT_DejaVu24   FONT_DejaVu40   FONT_DejaVu56   FONT_DejaVu72
    FONT_Minya      FONT_Small      FONT_Tooney     FONT_Ubuntu
    GREEN           GREENYELLOW     HSPI            JPG
    LANDSCAPE       LANDSCAPE_FLIP  LASTX           LASTY
    LIGHTGREY       M5STACK         M5STICK         MAGENTA
    MAROON          NAVY            OLIVE           ORANGE
    PINK            PORTRAIT        PORTRAIT_FLIP   PURPLE
    RED             RIGHT           VSPI            WHITE
    YELLOW          arc             attrib7seg      backlight
    circle          clearwin        compileFont     deinit
    drawCircle      drawLine        drawPixel       drawRect
    drawRoundRect   drawTriangle    ellipse         fill
    fillCircle      fillRect        fillRoundRect   fillScreen
    fillTriangle    font            fontSize        getCursor
    get_bg          get_fg          hsb2rgb         image
    init            line            lineByAngle     orient
    pixel           polygon         println         qrcode
    rect            resetwin        restorewin      roundrect
    savewin         screensize      setBrightness   setColor
    setCursor       setRotation     setTextColor    set_bg
    set_fg          setwin          text            textClear
    textWidth       text_x          text_y          tft_deselect
    tft_readcmd     tft_select      tft_setspeed    tft_writecmd
    tft_writecmddata                triangle        winsize
tft.init(tft.M5STICK, width=160, height=80, rst_pin=18, miso=19, mosi=15, clk=13, cs=5, dc=23, bgr=True, backl_on=1)
    I (962328) [TFTSPI]: attached display device, speed=8000000
    I (962328) [TFTSPI]: bus uses native pins: false
tft.clear(0xFF0000)   # fundo vermelho
tft.screensize()
    (80, 160)
```

- [botões A e B ](https://github.com/m5stack/UIFlow-Code/wiki/Event#button) (com algumas diferenças) :
```
from m5stack import btnA, btnB
btnA.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          pin             wasDoublePress
    pressFor        wasReleased     wasPressed      isPressed
    isReleased      restart         upDate          _event
    _eventLast      _valueLast      _pressTime      _releaseTime
    _doubleTime     _dbtime         _holdTime       _cbState
    _eventTime      cb
btnA.isPressed()    # sem pressionar o 1o botão frontal (abaixo da tela), botão A
    False
btnA.isPressed()    # pressionando o 1o botão frontal (abaixo da tela), botão A 
    True
btnA.wasPressed(lambda: print('button A pressed'))               # ativa interrupção ao pressionar o botão A, mostrando texto no terminal
btnA.wasReleased(lambda: print('button A released'))             # ativa interrupção ao liberar o botão A, mostrando texto no terminal
btnB.wasDoublePress(lambda: print('button B double released'))   # ativa interrupção ao pressionar duas vezes o botão B, mostrando texto no terminal
```

- ligar/desligar LED vermelho (pino G10), não é documentada em UIFlow, mas UIFlow-Desktop-IDE ou online permite ver exemplos 
simples em Blockly->MicroPython :
```
from m5stack import M5Led
M5Led.
    __class__       __init__        __module__      __qualname__
    __dict__        off             on              pin
M5Led.pin
    Pin(10)
M5Led.on()
M5Led.off()
```

- transmissor IR está conectado ao pino G9 como open drain, [vide documentação do M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) : :
```
from machine import Pin
ir = Pin(9, Pin.OPEN_DRAIN)
ir.
    __class__       value           IN              IRQ_FALLING
    IRQ_RISING      OPEN_DRAIN      OUT             PULL_DOWN
    PULL_HOLD       PULL_UP         WAKE_HIGH       WAKE_LOW
    init            irq             off             on
ir.value()    # IR desligado
    1
ir.value(0)   # IR ligado
```

- gerenciador de energia (PMIC) AXP192 não é documentado em UIFlow, mas UIFlow-Desktop-IDE ou online permite ver exemplos 
simples em Blockly->MicroPython :
```
from m5stack import axp
    I (129220) [TFTSPI]: attached display device, speed=8000000
    I (129220) [TFTSPI]: bus uses native pins: false
    [ M5 ] node id:d8a01d576c58, api key:D0FA494F
axp.
    __class__       __init__        __module__      __qualname__
    __dict__        addr            deinit          i2c
    _regChar        CURRENT_100MA   CURRENT_190MA   CURRENT_280MA
    CURRENT_360MA   CURRENT_450MA   CURRENT_550MA   CURRENT_630MA
    CURRENT_700MA   powerAll        getChargeState  setChargeState
    getBatVoltage   getBatCurrent   getVinVoltage   getVinCurrent
    getVBusVoltage  getVBusCurrent  getTempInAXP192
    powerOff        setLDO2Volt     setLDO3Volt     setLDO2State
    setLDO3State    setLcdBrightness                setChargeCurrent
    btnState        _read12Bit      _read13Bit      _read16Bit
axp.getVBusVoltage()
    5.0575   # V, USB
axp.getVBusCurrent()
    69.0   # mA
axp.getBatVoltage()
    4.1536   # V, bateria carregada
axp.getBatCurrent()
    0.0      # mA
axp.getTempInAXP192()
    39.5     # C
axp.setLcdBrightness(30)   # 0-100
axp.powerOff()   # Desliga
```

- acelerômetro/giroscópio MPU6886, não é documentado em UIFlow, mas UIFlow-Desktop-IDE ou online permite ver exemplos 
simples em Blockly->MicroPython :
```
import imu
imu.
    __class__       __name__        __file__        i2c_bus
    mpu6050         sh200q          SH200Q_ADDR     MPU6050_ADDR
    imu_i2c         IMU
imu0 = imu.IMU()
imu0.
    __class__       __enter__       __exit__        __init__
    __module__      __qualname__    __dict__        address
    i2c             _register_char  acceleration    gyro
    ypr             _accel_fs       _gyro_fs        preInterval
    accCoef         gyroCoef        angleGyroX      angleGyroY
    angleGyroZ      angleX          angleZ          angleY
    gyroXoffset     gyroYoffset     gyroZoffset     _accel_so
    _gyro_so        _register_short                 setGyroOffsets
    whoami          _register_three_shorts          _accel_sf
    _gyro_sf
imu0.whoami
    25
imu0.acceleration
    (-0.036, 0.036, 1.041)   # in g, M5Stick na horizontal
imu0.gyro
    (-2.687, 1.649, 12.412)
```

- RTC BM8563 é de origem chinesa, não tem documentação em inglês, só [datasheet em chinês na documentação do M5StickC](https://docs.m5stack.com/#/en/core/m5stickc). 
Não tem código-fonte, nem exemplos, em UIFlow/MicroPython, mas tem [em Arduino](https://github.com/m5stack/M5StickC).  
Vide na [documentação do M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) que o RTC BM8563 é alimentado pelo AXP192, ou seja, por 
bateria interna, cabo USB-C ou entrada de 5V. Logo **o RTC BM8563 mantém hora e data mesmo com o ESP32 desligado, basta ter carga na bateria do M5StickC**.  
O [pino de interrupção está conectado ao GPIO35 do ESP32](https://github.com/m5stack/M5StickC/issues/45), mas os drivers Arduino e UIFlow/MicroPython 
não têm função de alarme.
```
import hw
hw.
    __class__       __name__        __path__        axp192
    bm8563          _led
hw.bm8563.
    __class__       __name__        const           __file__
    i2c_bus         Bm8563
rtc_bm8563 = hw.bm8563.Bm8563()
rtc_bm8563.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          i2c             _updateTime
    setTime         now             year            month
    day             hour            minute          second
    week
rtc_bm8563.now()
    (2000, 0, 0, 20, 13, 0)   # (year, month, day, hour, minute, second)
rtc_bm8563.setTime(2020, 3, 21, 14, 33, 0)
rtc_bm8563.now()
    (2020, 3, 21, 14, 33, 3)
```

- RTC nativo de MicroPython oficial para ESP32, vide [exemplo](http://docs.micropython.org/en/v1.10/esp32/quickref.html#real-time-clock-rtc), 
sendo que não segue o padarão de [RTC do MicroPython oficial](https://docs.micropython.org/en/latest/library/machine.RTC.html), não tendo 
alarme, etc.  
'RTC.datetime()' precisa ter 8 argumentos que não são bem documentados, mas vide essa 
[GitHub issue 'RTC cannot be set correctly on ESP 32 #4540'](https://github.com/micropython/micropython/issues/4540#issuecomment-580965520),
notando que dia da semana (0-6 começando com 2a-feira) é desprezado pois é calculado automaticamente.  
**O RTC nativo do ESP32 não mantém hora e data se o ESP32 for desligado (apertando por 6s o botão lateral esquerdo para desligar o M5StickC)**.
```
from machine import RTC
rtc = RTC()
rtc.
    __class__       datetime        init            memory
rtc.datetime()
    (2000, 1, 1, 5, 0, 15, 23, 883318)
rtc.datetime((2020, 3, 21, 0, 12, 48, 0, 0)) # (year, month, day, day of week, hour, minute, second, microsecond, tzinfo)

```

- [módulo 'i2c_bus'](https://github.com/m5stack/UIFlow-Code/wiki/Advanced#i2c) de UIFlow, com barramento I2C listando 
AXP192 (power manager), BM8563 (RTC) e MPU6886 (accel + gyro) :
```
import i2c_bus
i2c = i2c_bus.get(i2c_bus.M_BUS)
i2c
    I2C (Port=1, Mode=MASTER, Speed=100000 Hz, sda=21, scl=22)
i2c.scan()
    [52, 81, 104]   # default I2C address of AXP192 is 0x34 = 52, BM8563 is 0x51 = 81, MPU6886 is 0x68 = 104
```

- também se pode inicializar I2C via ['machine.I2C'](https://docs.micropython.org/en/latest/esp32/quickref.html#i2c-bus), forma 
padrão do MicroPython para ESP32. Com barramento I2C listando AXP192 (power manager), BM8563 (RTC) e MPU6886 (accel + gyro) :
```
from machine import I2C
i2c = I2C(freq=400000, sda=21, scl=22)
i2c
    I2C (Port=0, Mode=MASTER, Speed=400000 Hz, sda=21, scl=22)
i2c.scan()
    [52, 81, 104]   # default I2C address of AXP192 is 0x34 = 52, BM8563 is 0x51 = 81, MPU6886 is 0x68 = 104
```

<img src="https://docs.m5stack.com/assets/img/product_pics/hat/env_hat/env_hat_01.jpg" alt="M5StickC C-Hat ENV" width="300"/>

- [C-Hat ENV](https://docs.m5stack.com/#/en/hat/hat-env), similar a [Unit ENV](https://docs.m5stack.com/#/en/unit/env) 
(mas esse não tem o magnetômetro BMM150) em termos de [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#env) e 
[código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_env.py).
O driver UIFlow/MicroPython para C-Hat ENV não inclui suporte ao magnetômetro BMM150, enquanto que o driver Arduino suporta. 
Seria interessante a alternativa de usar drivers mais completos para DHT12, BMP280 e BMM150, permitindo configurar e acessar 
todos os recursos desses sensores.
```
import time
import hat
hat_env = hat.get(hat.ENV)
hat_env.
    __class__       __init__        __module__      __qualname__
    values          __dict__        data            deinit
    time            _available      pressure        temperature
    humidity        dht12           bmp280          i2c
hat_env.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=0, scl=26)   # different I2C bus, specific for C-Hat's
hat_env.i2c.scan()
    [16, 92, 118]       #  DHT12 default I2C address is 0x5C = 92, BMP280 is 0x76 = 118, BMM150 is 0x10 = 16
while True:
    print('T = {:.1f} C, pressure = {:.3f} hPa, humidity = {:.1f} '.format(hat_env.temperature, hat_env.pressure, hat_env.humidity))
    time.sleep_ms(1000)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/hat/spk_hat/spk_hat_01.jpg" alt="M5StickC C-Hat SPK" width="300"/>

- [C-Hat SPK](https://docs.m5stack.com/#/en/hat/hat-spk), similar ao [speakder de M5Stack Core](https://docs.m5stack.com/#/en/unit/env) 
 em termos de [exemplos](https://https://github.com/m5stack/UIFlow-Code/blob/master/lib/speak.py).
```
import hat
hat_spk = hat.get(hat.SPEAKER)
hat_spk.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          pin             _timer
    checkInit       _timeout_cb     tone            sing
    setBeat         setVolume       pwm_init        pin_en
    _volume         _beat_time
hat_spk.tone(600, 500)   # f = 600 Hz, T = 500 ms
hat_spk.setVolume(3)     # default parece ser 2, parece que varia de 0-5.
hat_spk.sing(600, 1)     # f = 600 Hz, 1 beat = 500 ms
for i in range(10):
    hat_spk.tone(300 + i*60, 500)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/unit/tof/unit_tof_01.jpg" alt="M5Stack Unit ToF VL53L0X" width="300"/>

- [Unit TOF VL53L0X](https://docs.m5stack.com/#/en/unit/tof), [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#tof) e 
[código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_tof.py), mostra 70 atualizações/leituras por segundo.  
Seria interessante a alternativa de usar driver mais completo para VL53L0X, permitindo configurar e acessar todos os recursos desse sensor.
```
import time
import unit
unit_tof = unit.get(unit.TOF, unit.PORTA)
unit_tof.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          distance        timer
    state           _available      i2c             _register_char
    _update         _register_short
unit_tof.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=32, scl=33)   # different I2C bus, specific for the Grove connector
unit_tof.i2c.scan()
    [41]        #  VL53L0X default I2C address is 0x29 = 41
while True:
    print(unit_tof.distance)
    time.sleep_ms(200)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/hat/tof_hat/tof_hat_01.jpg" alt="M5StickC C-Hat ToF VL53L0X" width="300"/>

- [C-Hat TOF VL53L0X](https://docs.m5stack.com/#/en/hat/hat-tof), similar a [Unit TOF VL53L0X](https://docs.m5stack.com/#/en/unit/tof) 
em termos de [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#tof) e 
[código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_tof.py), que mostra 
70 atualizações/leituras por segundo.  
Seria interessante a alternativa de usar driver mais completo para VL53L0X, permitindo configurar e acessar todos os recursos desse sensor.
```
import time
import hat
hat_tof = hat.get(hat.TOF)
hat_tof.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          _available      i2c
    GetDistance
hat_tof.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=0, scl=26)   # different I2C bus, specific for C-Hat's
hat_tof.i2c.scan()
    [41]        #  VL53L0X default I2C address is 0x29 = 41
while True:
    print(hat_tof.GetDistance())
    time.sleep_ms(200)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/unit/M5GO_Unit_adc.png" alt="M5Stack Unit ADC 16 bits I2C ADS1100" width="300"/>

- [Unit ADC 16 bits ADS1100](https://docs.m5stack.com/#/en/unit/adc), [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#adc) e 
[código-fonte de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_adc.py), onde se vê que as opções 
(mode, rate, gain, etc) não estão implementadas nesse código-fonte.  
Default : offset = 0.25V, mode = continue (or single shot), rate = 15 samples/s (or 30/60/240), gain = 1x (or 2x/4x/8x). 
Tais valores discordam do [datasheet do ADS1100](https://www.ti.com/product/ADS1100), que cita 8/16/32/128 samples/s com resolução 
respectiva de 12/14/15/16 bits, default é 8 samples/s e resolução de 12 bits.  
Seria interessante a alternativa de usar driver mais completo para ADS1100, permitindo configurar e acessar todos os recursos desse sensor.
```
import unit
unit_adc = unit.get(unit.ADC, unit.PORTA)
unit_adc.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          mode            offset
    rate            _available      i2c             _write_u8
    _read_u16       measure_set     voltage         gain
    mini_code       gain_code
unit_adc.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=32, scl=33)   # different I2C bus, specific for the Grove connector
 unit_adc.i2c.scan()
    [72]           # ADS1100 default I2C address is 0x48 = 72
unit_adc.voltage   # get voltage, value range 0~12V
    3.222          # 3V3 of M5StickC (3.290 V on voltimeter)
    4.959          # 5V of M5StickC (5.065 V on voltimeter)
```

<img src="https://docs.m5stack.com/assets/img/product_pics/hat/adc_hat/adc_hat_01.jpg" alt="M5Stack C-Hat ADC 16 bits ADS1100" width="300"/>

- [C-Hat ADC 16 bits ADS1100](https://docs.m5stack.com/#/en/hat/hat-adc), similar a [Unit ADC 16 bits ADS1100](https://docs.m5stack.com/#/en/unit/dac) 
em termos de [exemplos](https://github.com/m5stack/UIFlow-Code/wiki/Unit#adc) e 
[código-fonte, de meados de 2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_adc.py), onde se vê que as opções (mode, rate, gain, etc) 
não estão implementadas nesse código-fonte.  
Default : offset = 0.25V, mode = continue (or single shot), rate = 15 samples/s (or 30/60/240), gain = 1x (or 2x/4x/8x).
Tais valores discordam do [datasheet do ADS1100](https://www.ti.com/product/ADS1100), que cita 8/16/32/128 samples/s com resolução 
respectiva de 12/14/15/16 bits, default é 8 samples/s e resolução de 12 bits.  
Seria interessante a alternativa de usar driver mais completo para ADS1100, permitindo configurar e acessar todos os recursos desse sensor.
```
import hat
hat_adc = hat.get(hat.ADC)
hat_adc.
    __class__       __init__        __module__      __qualname__
    __dict__        deinit          mode            offset
    rate            _available      i2c             _write_u8
    _read_u16       measure_set     rawData         voltage
    gain            mini_code       gain_code
hat_adc.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=0, scl=26)   # different I2C bus, specific for C-Hat's
hat_adc.i2c.scan()
    [72]           # ADS1100 default I2C address is 0x48 = 72
hat_adc.voltage    # get voltage, value range 0~12V
    5.031          # 5V of M5StickC (5.210 V on voltimeter)
```

- ADC nativo do ESP32 é de 12 bits, em MicroPython limitado aos pinos 32 a 39, sendo que [no M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) 
o pino G36 está exposto no barramento de 8 pinos para C-HAT's e os pinos G32 e G33 estão no conector Grove.  
Vide [documentação oficial de MicroPython para ESP32 usando ADC](https://docs.micropython.org/en/latest/esp32/quickref.html#adc-analog-to-digital-conversion).  
Mas ao que parece UIFlow usou partes de "MicroPython_ESP32_psRAM_LoBo" para implementar mais funções no ADC para ESP32, vide 
[documentação de ADC para MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/adc).  
Notar que **o ADC do ESP32 tem diversos problemas** : offset (60-130 mV), não-linearidade e alto ruído (logo a resolução efetiva é uns 
4-5 bits menor que a nominal), vide [comparação com ADS1115](https://github.com/bboser/IoT49/blob/master/doc/analog_io.md).
```
import machine
# Vref (internal reference voltage) calibration should be made on each ESP32
machine.ADC.vref()                # read the previous Vref
    (1114, False)
machine.ADC.vref(vref_topin=26)   # route the Vref to GPIO25, which should be measured with a voltmeter
    (1114, 26)
machine.ADC.vref(vref=1121)       # set the Vref to measured value in mV
    (1121, False)
machine.ADC.vref()
    (1121, False)
adc = machine.ADC(machine.Pin(36))
adc
    ADC(Pin(36): unit=ADC1, chan=0, width=12 bits, atten=0dB (1.1V), Vref=1114 mV)
adc.
    __class__       read            ATTN_0DB        ATTN_11DB
    ATTN_2_5DB      ATTN_6DB        HALL            WIDTH_10BIT
    WIDTH_11BIT     WIDTH_12BIT     WIDTH_9BIT      atten
    collect         collected       deinit          progress
    read_timed      readraw         stopcollect     vref
    width
adc.atten(machine.ADC.ATTN_11DB)    # change input attenuation to 11dB (i. e., 0.0-3.9 V)
    ADC(Pin(36): unit=ADC1, chan=0, width=12 bits, atten=11dB (3.9V), Vref=1114 mV)
adc.readraw()
    4095    # 4095 is full scale 
adc.read() 
    3145    # 3145 mV = 3.145 V, measured 3.290 V with voltimeter on GND and 3V3 (at C-Hat connector)
adc.readraw()
    0       # 0 is zero of scale
adc.read()
    142     # 142 mV = 0.142 V, measured 0.000V with voltimeter on GND and GND
adc.width(machine.ADC.WIDTH_10BIT)   # change resolution to 10 bits (0-1023)
    ADC(Pin(36): unit=ADC1, chan=0, width=10 bits, atten=11dB (3.9V), Vref=1114 mV)
adc.readraw()
    1024    # 1023 is full scale 
adc.read() 
    3143    # 3143 mV = 3.143 V, measured 3.290 V with voltimeter on GND and 3V3 (at C-Hat connector)
adc.readraw()
    0       # 0 is zero of scale
adc.read()
    142     # 142 mV = 0.142 V, measured 0.000V with voltimeter on GND and GND
```

<img src="https://docs.m5stack.com/assets/img/product_pics/hat/dac_hat/dac_hat_01.jpg" alt="M5Stack C-Hat DAC 12 bits I2C MCP4725" width="300"/>

- [C-Hat DAC MCP4725 12 bits](https://docs.m5stack.com/#/en/hat/hat-dac), similar a [Unit DAC MCP4725 12 bits](https://docs.m5stack.com/#/en/unit/dac) 
em termos de [exemplos (mas tem erro na citação do chip e endereço I2C)](https://github.com/m5stack/UIFlow-Code/wiki/Unit#dac) e [código-fonte, de meados de 
2019](https://github.com/m5stack/UIFlow-Code/blob/master/units/_dac.py), onde se vê que com 'save=True' o valor é salvo na EEPROM, que 'hold the DAC input code during power-off time, and the DAC output is available immediately 
after power-up'.  
Seria interessante a alternativa de usar driver mais completo para MCP4725, permitindo configurar e acessar todos os recursos desse sensor.
```
import hat
hat_dac = hat.get(hat.DAC)
hat_dac.i2c
    I2C (Port=0, Mode=MASTER, Speed=100000 Hz, sda=0, scl=26)   # different I2C bus, specific for C-Hat's
hat_adc.i2c.scan()
    [96]           # MCP4725 default I2C address is 0x60 = 96
hat_dac.writeData(4095)   # 0-4095 proportional to 0.0-3.3V; save: True or False, whether to save to EEPROM
hat_dac.setVoltage(2.0)   # 0-3.3V; save: True or False, whether to save to EEPROM
```

- DAC nativo do ESP32 é de 8 bits em 2 pinos (G25 e G26), sendo que o pino G26 está exposto no barramento de 8 pinos do M5StickC para C-HAT's.
A documentação oficial de MicroPython para ESP32 ainda não cita DAC, dentro do módulo 'machine', mas a [documentação para 
pyb.DAC](https://docs.micropython.org/en/latest/library/pyb.DAC.html) pode ser em parte aproveitada.  
Mas ao que parece UIFlow usou partes de "MicroPython_ESP32_psRAM_LoBo" para implementar mais funções no DAC para ESP32, vide 
[documentação de DAC para MicroPython_ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki/dac).
```
import machine
dac = machine.DAC(machine.Pin(26))
dac.
    __class__       write           CIRCULAR        NOISE
    NORMAL          RAMP            SAWTOOTH        SINE
    TRIANGLE        beep            deinit          freq
    stopwave        waveform        wavplay         write_buffer
    write_timed
dac.write(0)     # 0.074 V measured with voltimeter on GND & G26
dac.write(64)    # 0.843 V
dac.write(128)   # 1.616 V
dac.write(192)   # 2.387 V
dac.write(255)   # 3.144 V, it should be = 3V3 pin (3.272 V on voltimeter)
```
- (FAZER) uso de microphone SPM1423, com driver externo :


### 1.3 Consumo de energia

#### 1.3.1 Medição de energia do 'M5StickC' usando [UM25C](https://www.aliexpress.com/item/32855845265.html) com cabo USB-C, mas pode estar carregando a bateria :

- 61 mA sem conectar via USB serial/terminal REPL (após 'Ctrl+A+K' no 'screen' para fechar o terminal), 72 mA com terminal REPL;

- desligando via botão power/reset (6s), mantém consumo de 5.8 mA (desativando terminal REPL antes) a 18 mA ! Parece que o 
CI gerenciador de energia AXP192 fica ativo para carregar bateria, etc, ao menos quando a alimentação USB é usada.

- 61/72 mA -> 82/94 mA com LED vermelho ligado :
```
from m5stack import M5Led
M5Led.on()
```

- 61/72 mA -> 89/100 mA com transmissor IR ligado :
```
from machine import Pin
ir = Pin(9, Pin.OPEN_DRAIN)
ir.value(0)    # IR ligado
```

- 61/72 mA -> 41/52 mA com 'axp.setLcdBrightness(0)', coloca a tela em modo de economia de energia :
```
from m5stack import axp
axp.setLcdBrightness(0)     # 52 mA (apagada)
axp.setLcdBrightness(25)    # 52 mA (bem escura)
axp.setLcdBrightness(50)    # 57 mA (brilho médio, legível)
axp.setLcdBrightness(75)    # 62 mA (brilho forte, bem legível)
axp.setLcdBrightness(100)   # 73 mA (brilho muito forte, muito legível)
```

- 61/72 mA -> 6/18 mA com 'axp.powerOff()', desliga ESP32, tela, etc, precisa ligar novamente via botão power :
```
from m5stack import axp
axp.powerOff()   # Desliga
```

- 61/72 mA -> 29/41 mA com 'machine.deepsleep(30000)', desliga só o ESP32 ficando a tela ligada, reinicializa em 30s :
```
import machine
machine.deepsleep(30000)   # a tela continua ligada
```

- 61/72 mA -> 8.6/21 mA com 'axp.setLcdBrightness(0)' e 'machine.deepsleep(30000)', coloca a tela em modo de economia de energia e 
desliga só o ESP32, reinicializa em 30s :
```
import machine
from m5stack import axp
axp.setLcdBrightness(0)   # 0-100
machine.deepsleep(30000)    
```

Consumo de energia por itens :  
- ESP32 : 32 mA em idle (sem rodar nada); 
- AXP192, etc : 6-18 mA (sem terminal/com terminal habilitado) ao desligar via 'axp.powerOff()' ou botão power/reset (6s);
- tela : 5 mA (brilho médio, legível), 21 mA (brilho muito forte, muito legível);
- LED vermelho : 22 mA;
- transmissor IR : 28 mA.



## 2. MicroPython oficial v1.12 ou mais recente

Vide [documentação oficial de MicroPython para ESP32](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html).

É firmware [MicroPython oficial mais recente, v1.12 de 12/2019](http://micropython.org/download), com código-fonte aberto (permite frozen 
modules, adicionar módulos em C, etc). Tem mais (111 kB x 83 kB) RAM livre que UIFlow/MicroPython, menor consumo de energia (ainda mais 
com underclocking). Mas :
* não reconhece a tela e sensores, pois tem poucos drivers (apa106, dht, ds18x20 e neopixel), então tem que instalar manualmente drivers de 
terceiros para a tela ST7735S, acelerômetro & giroscópio MPU6886, RTC BM8563 e CI gerenciador de energia AXP192, bem como para os vários 
dispositivos em 'Units' e 'C Hats'.


### 2.1 Instalando firmware :

Apagando flash, ligar pressionando o 3o botão (C) para entrar em modo 'flash' :  
`$ pip install esptool`  
`$ esptool.py -p /dev/ttyUSB0 -b 115200 --chip esp32 erase_flash`  
Instalando firmware [MicroPython v1.12 ou mais recente, oficial](http://micropython.org/download), para M5StickC, que não tem PSRAM :  
`$ esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash --flash_mode dio -z 0x1000 esp32-idf3-20191220-v1.12.bin`  


### 2.2 Testes e exemplos :

- tem só 1 arquivo em '/pyboard', 'boot.py' (só com comentários e comandos que podem ser habilitados para webrepl, etc), que 
pode ser copiado usando ['rshell'](https://github.com/dhylands/rshell) (mais fácil) ou ['ampy'](https://github.com/scientifichackers/ampy) :
```
$ rshell -p /dev/ttyUSB0
> ls -l /pyboard
       139 Jan  1 2000  boot.py
> cat /pyboard/boot.py
    # This file is executed on every boot (including wake-boot from deepsleep)
    #import esp
    #esp.osdebug(None)
    #import webrepl
    #webrepl.start()
```

- dando reset apertando uma vez o botão laterial esquerdo :
```
I (439) cpu_start: Pro cpu up.
I (439) cpu_start: Application information:
I (440) cpu_start: Compile time:     Dec 20 2019 07:50:41
I (443) cpu_start: ELF file SHA256:  0000000000000000...
I (449) cpu_start: ESP-IDF:          v3.3
I (453) cpu_start: Starting app cpu, entry point is 0x40083600
I (0) cpu_start: App cpu up.
I (464) heap_init: Initializing. RAM available for dynamic allocation:
I (471) heap_init: At 3FFAE6E0 len 00001920 (6 KiB): DRAM
I (477) heap_init: At 3FFBA488 len 00025B78 (150 KiB): DRAM
I (483) heap_init: At 3FFE0440 len 00003AE0 (14 KiB): D/IRAM
I (489) heap_init: At 3FFE4350 len 0001BCB0 (111 KiB): D/IRAM
I (496) heap_init: At 40092D6C len 0000D294 (52 KiB): IRAM
I (502) cpu_start: Pro cpu start user code
I (185) cpu_start: Chip Revision: 1
W (185) cpu_start: Chip revision is higher than the one configured in menuconfig. Suggest to upgrade it.
I (189) cpu_start: Starting scheduler on PRO CPU.
I (0) cpu_start: Starting scheduler on APP CPU.
MicroPython v1.12 on 2019-12-20; ESP32 module with ESP32
Type "help()" for more information.
```

- com 'boot.py' de 'MicroPython v1.12 on 2019-12-20' no M5StickC :
```
$ rshell -p /dev/ttyUSB0
> repl
MicroPython v1.12 on 2019-12-20; ESP32 module with ESP32
import gc
gc.collect()
gc.mem_free()
    114368   # 111.7 kB
```

- ver versão de MicroPython (após boot/reset também mostra) :
```
import os
os.uname()
    (sysname='esp32', nodename='esp32', release='1.12.0', version='v1.12 on 2019-12-20', machine='ESP32 module with ESP32')
```

- [módulo 'machine'](https://docs.micropython.org/en/latest/library/machine.html) e frequência do ESP32 (default é 160 MHz, 
pode ser alterada para 20MHz, 40MHz, 80Mhz, 160MHz or 240MHz) :
```
import machine
machine.freq()
    160000000
machine.freq(240000000)
machine.freq(160000000)
machine.freq(80000000)
machine.freq(40000000)
machine.freq(20000000)
```

- help :
```
help()
    Welcome to MicroPython on the ESP32!
    
    For generic online docs please visit http://docs.micropython.org/
    
    For access to the hardware use the 'machine' module:
    
    import machine
    pin12 = machine.Pin(12, machine.Pin.OUT)
    pin12.value(1)
    pin13 = machine.Pin(13, machine.Pin.IN, machine.Pin.PULL_UP)
    print(pin13.value())
    i2c = machine.I2C(scl=machine.Pin(21), sda=machine.Pin(22))
    i2c.scan()
    i2c.writeto(addr, b'1234')
    i2c.readfrom(addr, 4)
    
    Basic WiFi configuration:
    
    import network
    sta_if = network.WLAN(network.STA_IF); sta_if.active(True)
    sta_if.scan()                             # Scan for available access points
    sta_if.connect("<AP_name>", "<password>") # Connect to an AP
    sta_if.isconnected()                      # Check for successful connection
    
    Control commands:
      CTRL-A        -- on a blank line, enter raw REPL mode
      CTRL-B        -- on a blank line, enter normal REPL mode
      CTRL-C        -- interrupt a running program
      CTRL-D        -- on a blank line, do a soft reset of the board
      CTRL-E        -- on a blank line, enter paste mode
    
    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (drivers, etc, do UIFlow MicroPython firmware) :

```
help('modules')
    __main__          framebuf          ucryptolib        urandom
    _boot             gc                uctypes           ure
    _onewire          inisetup          uerrno            urequests
    _thread           machine           uhashlib          uselect
    _webrepl          math              uhashlib          usocket
    apa106            micropython       uheapq            ussl
    btree             neopixel          uio               ustruct
    builtins          network           ujson             utime
    cmath             ntptime           umqtt/robust      utimeq
    dht               onewire           umqtt/simple      uwebsocket
    ds18x20           sys               uos               uzlib
    esp               uarray            upip              webrepl
    esp32             ubinascii         upip_utarfile     webrepl_setup
    flashbdev         ucollections      upysh             websocket_helper
    Plus any modules on the filesystem
```

- [módulo 'esp'](https://docs.micropython.org/en/latest/library/esp.html) e tamanho da memória flash interna (4 MB) :
```
import esp
esp.
    __class__       __name__        LOG_DEBUG       LOG_ERROR
    LOG_INFO        LOG_NONE        LOG_VERBOSE     LOG_WARNING
    dht_readinto    flash_erase     flash_read      flash_size
    flash_user_start                flash_write     gpio_matrix_in
    gpio_matrix_out                 neopixel_write  osdebug
esp.flash_size()
    4194304   # 4MB
```

- [módulo 'esp32'](https://docs.micropython.org/en/latest/library/esp32.html), temperatura e leitura do sensor Hall do ESP32 :
```
import esp32
esp32.
    __class__       __name__        ULP             WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 hall_sensor     raw_temperature
    wake_on_ext0    wake_on_ext1    wake_on_touch
esp32.raw_temperature()   # in F
    123
(esp32.raw_temperature() - 32)/1.8   # in C
    50.55556
esp32.hall_sensor()
    4
```

- botões A e B estão conectados aos pinos G37 e G39, [vide documentação do M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) :
```
from machine import Pin
btnA = Pin(37, Pin.IN)
btnB = Pin(39, Pin.IN)
btnA.
    __class__       value           IN              IRQ_FALLING
    IRQ_RISING      OPEN_DRAIN      OUT             PULL_DOWN
    PULL_HOLD       PULL_UP         WAKE_HIGH       WAKE_LOW
    init            irq             off             on
btnA.value()    # sem pressionar o botão frontal (abaixo da tela), botão A
    1
btnA.value()    # pressionando o botão frontal (abaixo da tela), botão A 
    0
btnB.value()    # sem pressionar o botão laterial superior, botão B
    1
btnB.value()    # pressionando o botão laterial superior, botão B
    0
```

- LED vermelho está conectado ao pino G10 como open drain, [vide documentação do M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) : :
```
from machine import Pin
led = Pin(10, Pin.OPEN_DRAIN)
led.
    __class__       value           IN              IRQ_FALLING
    IRQ_RISING      OPEN_DRAIN      OUT             PULL_DOWN
    PULL_HOLD       PULL_UP         WAKE_HIGH       WAKE_LOW
    init            irq             off             on
led.value()    # LED apagado
    1
led.value(0)    # LED aceso
```

- transmissor IR está conectado ao pino G9 como open drain, [vide documentação do M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) : :
```
from machine import Pin
ir = Pin(9, Pin.OPEN_DRAIN)
ir.
    __class__       value           IN              IRQ_FALLING
    IRQ_RISING      OPEN_DRAIN      OUT             PULL_DOWN
    PULL_HOLD       PULL_UP         WAKE_HIGH       WAKE_LOW
    init            irq             off             on
ir.value()    # IR desligado
    1
ir.value(0)   # IR ligado
```

- gerenciador de energia (PMIC) AXP192. Copiar para '/pyboard/lib/' o driver 'axp192.py', baseado no driver do 
[repositório 'M5StickC-uPy'](https://github.com/karfas/M5StickC-uPy) que implementa quase todas as funções do submódulo 'axp' de 
UIFlow/MicroPython, porém com adição da função 'set_LCD_brightness(brightness)', baseada na função 'ScreenBreath' do 
[driver AXP192 para Arduino](https://github.com/m5stack/M5StickC/blob/master/src/AXP192.cpp) :
```
from machine import I2C, Pin
from axp192 import AXP192
i2c = I2C(freq=400000, sda=Pin(21), scl=Pin(22))
axp = AXP192(i2c)
axp.
    __class__       __init__        __module__      __qualname__
    __dict__        setup           temperature     set_LD02
    i2c             conf            _write          _read
    _read_bits      _set_power_0x12                 button
    battery_voltage                 battery_current
    input_voltage   input_current   bus_voltage     bus_current
    battery_power   battery_charge_current          aps_voltage
    warning_level   set_sleep
axp.setup()    # inicializa/energiza LCD backlight, RTC, MPU6886, etc
axp.bus_voltage()
    5.0405     # V, USB
axp.bus_current()
    55.125     # mA
axp.aps_voltage()
   4.9854      # V, ?
axp.battery_voltage()
    4.1371     # V, bateria carregada
axp.battery_current()
    0.0        # mA
axp.temperature()
    35.60001   # C
axp.set_LCD_Brightness(10)   # 0-12    
axp.set_LD02(False)   # desliga somente LCD backlight
axp.set_sleep()   # desliga ESP32, LCD backlight, RTC, MPU6886, etc
```

- RTC nativo de MicroPython oficial para ESP32, vide [exemplo](http://docs.micropython.org/en/v1.10/esp32/quickref.html#real-time-clock-rtc), 
sendo que não segue o padarão de [RTC do MicroPython oficial](https://docs.micropython.org/en/latest/library/machine.RTC.html), não tendo 
alarme, etc.  
'RTC.datetime()' precisa ter 8 argumentos que não são bem documentados, mas vide essa 
[GitHub issue 'RTC cannot be set correctly on ESP 32 #4540'](https://github.com/micropython/micropython/issues/4540#issuecomment-580965520),
notando que dia da semana (0-6 começando com 2a-feira) é desprezado pois é calculado automaticamente.  
**O RTC nativo do ESP32 não mantém hora e data se o ESP32 for desligado (apertando por 6s o botão lateral esquerdo para desligar o M5StickC)**.
```
from machine import RTC
rtc = RTC()
rtc.
    __class__       datetime        init            memory
rtc.datetime()
    (2000, 1, 1, 5, 0, 15, 23, 883318)
rtc.datetime((2020, 3, 21, 0, 12, 48, 0, 0)) # (year, month, day, day of week, hour, minute, second, microsecond, tzinfo)
```

- I2C via ['machine.I2C'](https://docs.micropython.org/en/latest/esp32/quickref.html#i2c-bus), forma padrão do MicroPython para ESP32. 
Com barramento I2C listando AXP192 (power manager), BM8563 (RTC) e MPU6886 (accel + gyro) :
```
from machine import I2C, Pin
i2c = I2C(freq=400000, sda=Pin(21), scl=Pin(22))
i2c.
    __class__       readinto        start           stop
    write           init            readfrom        readfrom_into
    readfrom_mem    readfrom_mem_into               scan
    writeto         writeto_mem     writevto
i2c.scan()
    [52, 81, 104]   # default I2C address of AXP192 is 0x34 = 52, BM8563 is 0x51 = 81, MPU6886 is 0x68 = 104
```

- ADC nativo do ESP32 é de 12 bits, em MicroPython limitado aos pinos 32 a 39, sendo que [no M5StickC](https://docs.m5stack.com/#/en/core/m5stickc) 
o pino G36 está exposto no barramento de 8 pinos para C-HAT's e os pinos G32 e G33 estão no conector Grove.  
Vide [documentação oficial de MicroPython para ESP32 usando ADC](https://docs.micropython.org/en/latest/esp32/quickref.html#adc-analog-to-digital-conversion).  
Notar que **o ADC do ESP32 tem diversos problemas** : offset (60-130 mV), não-linearidade e alto ruído (logo a resolução efetiva é uns 
4-5 bits menor que a nominal), vide [comparação com ADS1115](https://github.com/bboser/IoT49/blob/master/doc/analog_io.md).
```
import machine
machine.ADC.
    __class__       __name__        read            __bases__
    ATTN_0DB        ATTN_11DB       ATTN_2_5DB      ATTN_6DB
    WIDTH_10BIT     WIDTH_11BIT     WIDTH_12BIT     WIDTH_9BIT
    atten           read_u16        width
adc = machine.ADC(machine.Pin(36))
adc
    ADC(Pin(36))
adc.
    __class__       read            ATTN_0DB        ATTN_11DB
    ATTN_2_5DB      ATTN_6DB        WIDTH_10BIT     WIDTH_11BIT
    WIDTH_12BIT     WIDTH_9BIT      atten           read_u16
    width
adc.atten(machine.ADC.ATTN_11DB)    # change input attenuation to 11dB (i. e., 0.0-3.9 V)
adc.read()
    4095    # 4095 is full scale, connected to 3V3 
adc.read()
    0       # 0 is zero of scale, connected to GND
adc.width(machine.ADC.WIDTH_10BIT)   # change resolution to 10 bits (0-1023)
adc.read()
    1023    # 1023 is full scale, connected to 3V3  
adc.read()
    0       # 0 is zero of scale, connected to GND
```

- DAC nativo do ESP32 é de 8 bits em 2 pinos (G25 e G26), sendo que o pino G26 está exposto no barramento de 8 pinos do M5StickC para C-HAT's.
A documentação oficial de MicroPython para ESP32 ainda não cita DAC, dentro do módulo 'machine', mas a 
[documentação para pyb.DAC](https://docs.micropython.org/en/latest/library/pyb.DAC.html) pode ser em parte aproveitada, i. e., só comando 'write()'.  
```
import machine
dac = machine.DAC(machine.Pin(26))
dac.
    __class__       write
dac.write(0)     # 0.074 V measured with voltimeter on GND & G26
dac.write(64)    # 0.843 V
dac.write(128)   # 1.616 V
dac.write(192)   # 2.387 V
dac.write(255)   # 3.144 V, it should be = 3V3 pin (3.272 V on voltimeter)
```

- (FAZER) exemplo de uso de tela ST7735S com drivers externos;

- (FAZER) uso de MPU6882 com driver externo :

- (FAZER) uso de RTC BM8563 com driver externo :

- (FAZER) uso de microphone SPM1423, com driver externo :




### 2.3 Consumo de energia

#### 2.3.1 Medição de energia do 'M5StickC' usando [UM25C](https://www.aliexpress.com/item/32855845265.html) com cabo USB-C, mas pode estar carregando a bateria :

- 28 mA sem conectar via USB serial/terminal REPL (após 'Ctrl+A+K' no 'screen' para fechar o terminal), 40 mA com terminal REPL;

- desligando via botão power/reset (6s), mantém consumo de 5.8 mA (desativando terminal REPL antes) a 18 mA ! Parece que o 
CI gerenciador de energia AXP192 fica ativo para carregar bateria, etc, ao menos quando a alimentação USB é usada;

- 15-37/27-49 mA variando frequência do ESP32 entre 20 MHz, 40 MHz, 80 MHz, 160 MHz e 240 MHz :
```
import machine
machine.freq(240000000)   # 37/49 mA
machine.freq(160000000)   # default, 28/40 mA
machine.freq(80000000)    # 24/36 mA
machine.freq(40000000)    # 17/29 mA
machine.freq(20000000)    # 15/27 mA
```

- 28/40 mA -> 51/63 mA com LED vermelho ligado :
```
from machine import Pin
led = Pin(10, Pin.OPEN_DRAIN)
led.value(0)    # LED aceso
```

- 28/40 mA -> 56/68 mA com transmissor IR ligado :
```
from machine import Pin
ir = Pin(9, Pin.OPEN_DRAIN)
ir.value(0)    # IR ligado
```

- 28/40 mA -> 50/62 mA ao ligar AXP192 (& LCD backlight, RTC, MPU6886, etc) :
```
from machine import I2C, Pin
from axp192 import AXP192
i2c = I2C(freq=400000, sda=Pin(21), scl=Pin(22))
axp = AXP192(i2c)
axp.setup()    # inicializa/energiza LCD backlight, RTC, MPU6886, etc, i aumenta para 62 mA
axp.set_LD02(False)   # desliga somente LCD backlight, i cai para 42 mA
axp.set_sleep()   # desliga ESP32, LCD backlight, RTC, MPU6886, etc
```

- 50/62 mA -> 30/42 mA com 'axp.set_LCD_brightness(0)', coloca a tela em modo de economia de energia :
```
from machine import I2C, Pin
from axp192 import AXP192
i2c = I2C(freq=400000, sda=Pin(21), scl=Pin(22))
axp = AXP192(i2c)
axp.setup()    # inicializa/energiza LCD backlight, RTC, MPU6886, etc, i aumenta para 62 mA
axp.set_LCD_brightness(0)    # 42 mA (apagada)
axp.set_LCD_brightness(8)    # 43 mA (bem escura)
axp.set_LCD_brightness(9)    # 46 mA (brilho médio, legível)
axp.set_LCD_brightness(11)   # 51 mA (brilho forte, bem legível)
axp.set_LCD_brightness(12)   # 62 mA (brilho muito forte, muito legível)
```

- 28/40 mA -> 7/19 mA com 'machine.deepsleep(30000)', desliga só o ESP32, reinicializa em 30s :
```
import machine
machine.deepsleep(30000)
```

Consumo de energia por itens :  
- ESP32 : 9/11/18/22/31 mA em idle (sem rodar nada) @ 20/40/80/160/240 MHz; 
- AXP192, etc : 6-18 mA (sem terminal/com terminal habilitado) ao desligar via botão power/reset (6s), ou 7/19 mA em 'deepsleep()';
- tela : 20 mA com luz de fundo;
- LED vermelho : 23 mA;
- transmissor IR : 28 mA.
