# MicroPython v1.12 firmwares with ['ulab' module](https://github.com/v923z/micropython-ulab) for [ESP8266](https://www.espressif.com/en/products/socs/esp8266/overview)


## ulab

['ulab' MicroPython module](https://github.com/v923z/micropython-ulab) is a numpy-like array manipulation library, written in C (native  module) so it needs to be included in MicroPython/CircuitPython firmware to be used. More about 'ulab' module :  
- [official documentation](https://micropython-ulab.readthedocs.io/);
- [tutorial 'ulab: Crunch Numbers fast in CircuitPython. Use numpy-like commands to process data quickly' from Adafruit Learn](https://learn.adafruit.com/ulab-crunch-numbers-fast-with-circuitpython);
- [topic 'ulab, or what you will - numpy on bare metal' in MicroPython forum](https://forum.micropython.org/viewtopic.php?f=3&t=7005).


## Firmware features

The MicroPython firmwares here are named in the form :

```<esp8266>_<'ulab_'><optional board memory>_<version>_date>.bin``` 

where :

- 'ulab' means the [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a numpy-like array manipulation library, is included in the firmware;
- the board memory flash can be '1m' (1 MB) or >= 2 MB when not cited (default). '512k' (512 kB) board variant doesn't have enough flash space for 'ulab' module;
- sp/single precision/FP32 for float point numbers is always used, as MicroPython on ESP8266 doesn't support dp/double precision/FP64;
- there is no 'thread' support, i. e., no ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html), not allowing multithreading, as MicroPython on ESP8266 doesn't support it.

For example :  
```esp8266_ulab_1m_v1.12-658-g3bab891e5_2020-07-26.bin```  
means it is a v1.12-658-g3bab891e5 firmware, from July 26 2020, with ulab module included, double precision float point numbers enabled, for ESP8266 boards with 1 MB of flash.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here with 'ulab' module were compiled using :

- 'rcolistete' fork of [MicroPython source code](https://github.com/rcolistete/micropython/), branch ['esp8266_1m_compatible_ulab'](https://github.com/rcolistete/micropython/tree/esp8266_1m_compatible_ulab) (to allow 'ulab' compile and fit in the internal flash of ESP8266), v1.12-658-g3bab891e5 from the commit ['ESP8266 1MB made compatible with ulab module' - 3bab891e5...'](https://github.com/rcolistete/micropython/commit/3bab891e52748978171fea1eb07a98aab3ad72f8), in July 26 2020. Note that the firmware ESP8266 with 1 MB flash memory has filesystem reduced to 348 kB (from 396 kB) to allow the 'ulab' module; 
- the docker larsks/esp-open-sdk which includes ['esp-open-sdk from pfalcon repository](https://github.com/pfalcon/esp-open-sdk);
- [ulab source code](https://github.com/v923z/micropython-ulab), v0.54.0 from the [PR#153 - "numerical.c : fixed 'out' may be used uninitialized error in function…"](https://github.com/v923z/micropython-ulab/pull/153), in July 24 2020.

To build the MicroPython firmware for ESP8266 boards boards with 'ulab' module, look at the :  
- [instructions of MicroPython source code for ESP8266 boards](https://github.com/micropython/micropython/tree/master/ports/esp8266);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries);
- [instructions of 'ulab' repository](https://github.com/v923z/micropython-ulab#compiling).

So the 'make' commands are appended with 'USER_C_MODULES=../../../ulab all' to build 'ulab' module in firmware.

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [GENERIC GENERIC_1M], as GENERIC_512k doesn't have enough flash space for 'ulab' module. After building, the 'firmware-combined.bin' is placed at 'esp8266/build-$BOARD', which should be copied before the following build.
```
# ulab, GENERIC version is the default, for ESP8266 boards with >= 2 MB of flash :
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make clean
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8 USER_C_MODULES=../../../ulab all
# ulab, GENERIC_1M version is for ESP8266 boards with 1 MB of flash :
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make clean BOARD=GENERIC_1M
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8 BOARD=GENERIC_1M USER_C_MODULES=../../../ulab all
```
Loop for building firmwares with 'ulab' for 2 ESP8266 variants :
```
for BOARD in GENERIC GENERIC_1M; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8 BOARD=$BOARD USER_C_MODULES=../../../ulab all
done
```
After each building loop, the 'firmware-combined.bin' placed at ''esp8266/build-$BOARD' should be copied before the following loop.