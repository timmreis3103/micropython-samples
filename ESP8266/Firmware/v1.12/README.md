# MicroPython v1.12 firmwares for [ESP8266](https://www.espressif.com/en/products/socs/esp8266/overview)

## Firmware features

The MicroPython firmwares here are named in the form :  
```<esp8266>_<optional board memory>_<version>_date>.bin``` 
where ESP8266 MicroPython firmwares :

- can have board memory flash with '1m' (1 MB), '512k' (512 kB) or >= 2 MB when not cited (default);
- use sp/single precision/FP32  for float point numbers, not supporting dp/double precision/FP64;
- don't have 'thread' support, i. e., there is no ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html), not allowing multithreading.

For example :  
```esp8266_1m_v1.12-657-g37e1b5c89_2020-07-26.bin```  
means it is a v1.12-657-g37e1b5c89 firmware, from July 26 2020, for ESP8266 boards with 1 MB of flash.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here were compiled using :
- the [MicroPython source code](https://github.com/micropython/micropython), v1.12-657-g37e1b5c89 from the commit [g37e1b5c89...](https://github.com/rcolistete/micropython/commit/37e1b5c891f9964bb6c95228bc2d718511507a69), in July 24 2020;
- the docker larsks/esp-open-sdk which includes ['esp-open-sdk from pfalcon repository](https://github.com/pfalcon/esp-open-sdk).

To build the MicroPython firmware for ESP8266 boards, look at the :  
- [instructions of MicroPython source code for ESP8266 boards](https://github.com/micropython/micropython/tree/master/ports/esp8266);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries).

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [GENERIC GENERIC_1M GENERIC_512K]. After building, the 'firmware-combined.bin' is placed at 'esp8266/build-$BOARD', which should be copied before the following build.
```
# GENERIC version is the default, for ESP8266 boards with >= 2 MB of flash :
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make clean
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8
# GENERIC_1M version is for ESP8266 boards with 1 MB of flash :
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make clean BOARD=GENERIC_1M
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8 BOARD=GENERIC_1M
# GENERIC_512K version is for ESP8266 boards with 512 kB of flash :
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make clean BOARD=GENERIC_512K
[esp8266]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8 BOARD=GENERIC_512
```
Loop for building firmwares for 3 ESP8266 variants :
```
for BOARD in GENERIC GENERIC_1M GENERIC_512K; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD larsks/esp-open-sdk make -j8 BOARD=$BOARD
done
```
After each building loop, the 'firmware-combined.bin' placed at ''esp8266/build-$BOARD' should be copied before the following loop.
