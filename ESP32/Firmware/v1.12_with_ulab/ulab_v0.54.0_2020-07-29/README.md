# MicroPython v1.12 firmwares **with ['ulab' module](https://github.com/v923z/micropython-ulab)** for [ESP32](http://esp32.net/)

## ulab

['ulab' MicroPython module](https://github.com/v923z/micropython-ulab) is a numpy-like array manipulation library, written in C (native  module) so it needs to be included in MicroPython/CircuitPython firmware to be used. More about 'ulab' module : 
- [official documentation](https://micropython-ulab.readthedocs.io/);
- [tutorial 'ulab: Crunch Numbers fast in CircuitPython. Use numpy-like commands to process data quickly' from Adafruit Learn](https://learn.adafruit.com/ulab-crunch-numbers-fast-with-circuitpython);
- [topic 'ulab, or what you will - numpy on bare metal' in MicroPython forum](https://forum.micropython.org/viewtopic.php?f=3&t=7005).

## Firmware features

The 16 MicroPython firmwares here are named in the form :  
```<ESP32 board name>_<'idf3'/'idf4'>_<optional 'ulab_'><'sp' or 'dp'>_<'thread'>_<version>_<date>.dfu``` 
where :
- [ESP32 board](http://esp32.net/) name can be :
  - `esp32`, for the common ESP32 board, with 520 kB of internal RAM and 4 MB of QSPI flash;
  - `esp32-d2wd`, for the boards with ESP32 D2WD, with 520 kB of internal RAM and 2 MB of embedded QSPI flash;
  -  `esp32-spiram`, for the ESP32 boards with 4 MB of external PSRAM/SPIRAM (using QSPI bus) and 4 MB of QSPI flash;
  - `tinypico`, for the [TinyPico ESP32 boards](https://www.tinypico.com/) with 4 MB of external PSRAM/SPIRAM (using QSPI bus) and 4 MB of QSPI flash;
- 'idf3' means firmware built with [ESP-IDF](https://github.com/espressif/esp-idf) v3.3, with support for BLE, LAN and PPP. 'idf4' means firmware built with [ESP-IDF](https://github.com/espressif/esp-idf) v4.0, with support for BLE, but no LAN or PPP;
- 'ulab' means the [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a NumPy-like array manipulation library, is included in the firmware;
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading, and is always used by all ESP32 MicroPython firmwares, here and the official ones;

For example :  
```esp32-spiram_idf4_ulab_dp_thread_v1.12-663-gea4670d5a_2020-07-29.bin```  
means it is a v1.12-663-gea4670d5a firmware, from July 29 2020, built with ESP-IDF v4.0, with ulab module included, double precision float point numbers and threads enabled, for ESP32 generic board with 4 MB PSRAM (SPIRAM) and 4 MB of flash memory.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here with 'ulab' module were compiled using :
- 'rcolistete' fork of [MicroPython source code](https://github.com/rcolistete/micropython/), branch ['esp32_FLOAT_option_no_error'](https://github.com/rcolistete/micropython/tree/esp32_FLOAT_option_no_error) (to allow ulab compile without errors, and option MICROPY_FLOAT_IMPL on ESP32, so it is simpler to build ESP32 firmware with single or double precision float point numbers, which is also [PR#6294](https://github.com/micropython/micropython/pull/6294")), v1.12-663-gea4670d5a from the commit ['ESP32 : make option MICROPY_FLOAT_IMPL and no float warnings' - ea4670d5a...'](https://github.com/rcolistete/micropython/commit/ea4670d5a63ed7fbb023eba6ab44e1427217b1b0), in July 28 2020;
- the docker [alelec/esp32-toolchain](https://gitlab.com/alelec/docker-esp32-toolchain/raw/master/Dockerfile) which includes ESP-IDF v3.3 or v4.0, depending on git hash used to build, see [this tips (but without the "alias esp32)"](https://forum.micropython.org/viewtopic.php?f=18&t=7138&sid=59eb917b1cd04494b47f7935f4bc768d&start=10#p41455), so that `esp32-idf` image (with ESP-IDF v3.3) and `esp32-idf4`image (with ESP-IDF v4.0) were built;
- [ulab source code](https://github.com/v923z/micropython-ulab), v0.54.0 from the [PR#153 - "numerical.c : fixed 'out' may be used uninitialized error in function…"](https://github.com/v923z/micropython-ulab/pull/153), in July 24 2020.

To build the MicroPython firmware for ESP32 boards, look at the :  
- [instructions of MicroPython source code for ESP32 boards](https://github.com/micropython/micropython/tree/master/ports/esp32);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries);
-  [instructions of 'ulab' repository](https://github.com/v923z/micropython-ulab#compiling).

So the 'make' commands are appended with 'USER_C_MODULES=../../../ulab all' to build 'ulab' module in firmware.

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [GENERIC GENERIC_D2WD GENERIC_OTA GENERIC_SPIRAM TINYPICO]. But I haven't tested GENERIC_OTA. After building, the 'firmware.bin' is placed at 'esp32/build-$BOARD', which should be copied before the following build for the same board.
```
v4.0 :# GENERIC and sp version is the default, using ESP-IDF v3.3 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 USER_C_MODULES=../../../ulab all
# sp version is the default for [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v3.3 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 BOARD=GENERIC_SPIRAM USER_C_MODULES=../../../ulab all
# dp version, BOARD can be [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v3.3 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 MICROPY_FLOAT_IMPL=double BOARD=GENERIC_SPIRAM USER_C_MODULES=../../../ulab all
# GENERIC and sp version is the default, using ESP-IDF v4.0 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 USER_C_MODULES=../../../ulab all
# sp version is the default for [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v4.0 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 BOARD=GENERIC_SPIRAM USER_C_MODULES=../../../ulab all
# dp version, BOARD can be [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v4.0 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 MICROPY_FLOAT_IMPL=double BOARD=GENERIC_SPIRAM USER_C_MODULES=../../../ulab all
```
Loops for building firmwares with ulab, 'sp'/'dp', ESP-IDF v3.3/4.0, for 4 types of ESP32 :
```
# sp, using ESP-IDF v3.3 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 BOARD=$BOARD USER_C_MODULES=../../../ulab all
done
# dp, using ESP-IDF v3.3 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8  MICROPY_FLOAT_IMPL=double BOARD=$BOARD USER_C_MODULES=../../../ulab all
done
# sp, using ESP-IDF v4.0 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 BOARD=$BOARD USER_C_MODULES=../../../ulab all
done
# dp, using ESP-IDF v4.0 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8  MICROPY_FLOAT_IMPL=double BOARD=$BOARD USER_C_MODULES=../../../ulab all
done
```
After each building loop, the 'firmware.bin' placed at 'esp32/build-$BOARD' should be copied before the following loop.
