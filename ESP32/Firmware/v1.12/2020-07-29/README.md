# MicroPython v1.12 firmwares for [ESP32](http://esp32.net/)

## Firmware features

The 16 MicroPython firmwares here are named in the form :  
```<ESP32 board name>_<'idf3'/'idf4'>_<'sp' or 'dp'>_<'thread'>_<version>_<date>.dfu```  
where :
- [ESP32 board](http://esp32.net/) name can be :
  - `esp32`, for the common ESP32 board, with 520 kB of internal RAM and 4 MB of QSPI flash;
  - `esp32-d2wd`, for the boards with ESP32 D2WD, with 520 kB of internal RAM and 2 MB of embedded QSPI flash;
  -  `esp32-spiram`, for the ESP32 boards with 4 MB of external PSRAM/SPIRAM (using QSPI bus) and 4 MB of QSPI flash;
  - `tinypico`, for the [TinyPico ESP32 boards](https://www.tinypico.com/) with 4 MB of external PSRAM/SPIRAM (using QSPI bus) and 4 MB of QSPI flash;
- 'idf3' means firmware built with [ESP-IDF](https://github.com/espressif/esp-idf) v3.3, with support for BLE, LAN and PPP. 'idf4' means firmware built with [ESP-IDF](https://github.com/espressif/esp-idf) v4.0, with support for BLE, but no LAN or PPP;
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading, and is always used by all ESP32 MicroPython firmwares, here and the official ones;

For example :  
```esp32-spiram_idf4_dp_thread_v1.12-663-g693e7395b_2020-07-29.bin```  
means it is a v1.12-663-g693e7395b firmware, from July 29 2020, built with ESP-IDF v4.0, with double precision float point numbers and threads enabled, for ESP32 generic board with 4 MB PSRAM (SPIRAM) and 4 MB of flash memory.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here were compiled using :
- 'rcolistete' fork of [MicroPython source code](https://github.com/rcolistete/micropython/), branch ['esp32_FLOAT_option'](https://github.com/rcolistete/micropython/tree/esp32_FLOAT_option) (to allow option MICROPY_FLOAT_IMPL on ESP32, so it is simpler to build ESP32 firmware with single or double precision float point numbers, which is also [PR#6294](https://github.com/micropython/micropython/pull/6294")), v1.12-663-g693e7395b from the commit ['ESP32 : make option MICROPY_FLOAT_IMPL' - 693e7395b...'](https://github.com/rcolistete/micropython/commit/693e7395b0c0c9e9add073dc2e8786b4a53bba97), in July 28 2020;
- the docker [alelec/esp32-toolchain](https://gitlab.com/alelec/docker-esp32-toolchain/raw/master/Dockerfile) which includes ESP-IDF v3.3 or v4.0, depending on git hash used to build, see [this tips (but without the "alias esp32)"](https://forum.micropython.org/viewtopic.php?f=18&t=7138&sid=59eb917b1cd04494b47f7935f4bc768d&start=10#p41455), so that `esp32-idf` image (with ESP-IDF v3.3) and `esp32-idf4`image  (with ESP-IDF v4.0) were built.

To build the MicroPython firmware for ESP32 boards, look at the :  
- [instructions of MicroPython source code for ESP32 boards](https://github.com/micropython/micropython/tree/master/ports/esp32);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries).

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [GENERIC GENERIC_D2WD GENERIC_OTA GENERIC_SPIRAM TINYPICO]. But I haven't tested GENERIC_OTA. After building, the 'firmware.bin' is placed at 'esp32/build-$BOARD', which should be copied before the following build for the same board.
```
v4.0 :# GENERIC and sp version is the default, using ESP-IDF v3.3 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8
# sp version is the default for [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v3.3 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 BOARD=GENERIC_SPIRAM
# dp version, BOARD can be [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v3.3 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 MICROPY_FLOAT_IMPL=double BOARD=GENERIC_SPIRAM
# GENERIC and sp version is the default, using ESP-IDF v4.0 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8
# sp version is the default for [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v4.0 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 BOARD=GENERIC_SPIRAM
# dp version, BOARD can be [GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO], using ESP-IDF v4.0 :
[esp32]$ docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 MICROPY_FLOAT_IMPL=double BOARD=GENERIC_SPIRAM
```
Loops for building firmwares with 'sp'/'dp', ESP-IDF v3.3/4.0, for 4 types of ESP32 :
```
# sp, using ESP-IDF v3.3 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8 BOARD=$BOARD
done
# dp, using ESP-IDF v3.3 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf make -j8  MICROPY_FLOAT_IMPL=double BOARD=$BOARD
done
# sp, using ESP-IDF v4.0 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8 BOARD=$BOARD
done
# dp, using ESP-IDF v4.0 :
for BOARD in GENERIC GENERIC_D2WD GENERIC_SPIRAM TINYPICO; do
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make BOARD=$BOARD clean
    docker run --rm -v $HOME:$HOME -u $UID -w $PWD esp32-idf4 make -j8  MICROPY_FLOAT_IMPL=double BOARD=$BOARD
done
```
After each building loop, the 'firmware.bin' placed at 'esp32/build-$BOARD' should be copied before the following loop.
