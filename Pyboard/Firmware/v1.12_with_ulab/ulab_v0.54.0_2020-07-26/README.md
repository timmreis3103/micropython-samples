# MicroPython v1.12 firmwares with ['ulab' module](https://github.com/v923z/micropython-ulab) for [Pyboard v1.1](https://store.micropython.org/product/PYBv1.1)/[Lite v1.0](https://store.micropython.org/product/PYBLITEv1.0)/[D SF2](https://store.micropython.org/product/PYBD-SF2-W4F2)/[D SF3](https://store.micropython.org/product/PYBD-SF3-W4F2)/[D SF6](https://store.micropython.org/product/PYBD-SF6-W4F2)


## ulab

['ulab' MicroPython module](https://github.com/v923z/micropython-ulab) is a numpy-like array manipulation library, written in C (native  module) so it needs to be included in MicroPython/CircuitPython firmware to be used. More about 'ulab' module :  
- [official documentation](https://micropython-ulab.readthedocs.io/);
- [tutorial 'ulab: Crunch Numbers fast in CircuitPython. Use numpy-like commands to process data quickly' from Adafruit Learn](https://learn.adafruit.com/ulab-crunch-numbers-fast-with-circuitpython);
- [topic 'ulab, or what you will - numpy on bare metal' in MicroPython forum](https://forum.micropython.org/viewtopic.php?f=3&t=7005).


## Firmware features

The 23 MicroPython firmwares here are named in the form :  
```<Pyboard name>_<ulab>_<'sp_' or 'dp_'><optional 'thread_' or 'network_'><version>_<date>.dfu```   
where :

- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading;
- 'network' means Pyboard v1.1/Lite v1.0 firmware with drivers for [CC3000 WiFi modules](https://docs.micropython.org/en/latest/library/network.CC3K.html) and [WIZnet5x00 Ethernet modules](https://docs.micropython.org/en/latest/library/network.WIZNET5K.html).

For example :  
```pybd-sf6_ulab_dp_thread_v1.12-658-gdf37e3fab_2020-07-26.dfu```  
means it is a v1.12-658-gdf37e3fab firmware, from July 26 2020, with ulab module included, double precision float point numbers and threads enabled, for Pyboard D SF6.

Pyboard Lite v1.0 firmware with ulab, FP64 and network is not possible due to the small flash (512 kB) of Pyboard Lite v1.0, unless you disable ulab sub-modules, but the choice depends on each one's use.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here were compiled using :
- 'rcolistete' fork of [MicroPython source code](https://github.com/rcolistete/micropython/), branch 
['pyb_sf2_frozen_2MB'](https://github.com/rcolistete/micropython/tree/pyb_sf2_frozen_2MB), to allow 'ulab' in single precision/FP32 fit in the small internal flash of Pyboard D SF2/SF3, v1.12-662-gf8531fe04 from the commit ['PYB_SF2 : .py frozen modules moved to external flash- 57333f6ea...'](https://github.com/rcolistete/micropython/commit/57333f6ea6660366f5497027b1b9b6905505b9eb), in July 14 2020, branch updated in July 26 2020;
- 'rcolistete' fork of [MicroPython source code](https://github.com/rcolistete/micropython/),  branch ['pyb_sf2_frozen_ulab_2'](https://github.com/rcolistete/micropython/tree/pyb_sf2_frozen_ulab_2MB), to allow 'ulab' in double precision fit almost all the sub-modules (except 'compare' and 'user') in the small internal flash of Pyboard D SF2/SF3,  v1.12-658-gdf37e3fab from the commit ['PYB_SF2 : .py frozen modules and ulab submodules moved to external flash - df37e3fab...'](https://github.com/rcolistete/micropython/commit/df37e3fab326635fd00c2f282eb5d662a3b2a650), in July 26 2020;
- [ulab source code](https://github.com/v923z/micropython-ulab), v0.54.0 from the [PR#153 - "numerical.c : fixed 'out' may be used uninitialized error in function…"](https://github.com/v923z/micropython-ulab/pull/153), in July 24 2020.
- gcc-arm cross-compiler ['gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2'](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads). Newer versions were not used because they build larger firmwares.

To build the MicroPython firmware for STM32 boards with 'ulab' module, look at the :  
- [instructions of MicroPython source code for STM32 boards](https://github.com/micropython/micropython/tree/master/ports/stm32);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries);
- [instructions of 'ulab' repository](https://github.com/v923z/micropython-ulab#compiling).

So the 'make' commands are appended with 'USER_C_MODULES=../../../ulab all' to build 'ulab' module in firmware.

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6]. After building, the 'firmware.dfu' is placed at 'stm32/build-$BOARD', which should be copied before the following build.

For Pyboard D SF2/SF3, remember to use the branches & commits cited above, ['pyb_sf2_frozen_2MB'](https://github.com/rcolistete/micropython/tree/pyb_sf2_frozen_2MB) and ['pyb_sf2_frozen_ulab_2'](https://github.com/rcolistete/micropython/tree/pyb_sf2_frozen_ulab_2MB).

```
# ulab and sp version (is the default for [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3]) :
[stm32]$ make -j8 BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# ulab and dp version (is the default for PYBD_SF6) :
[stm32]$ make -j8 BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab dp version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=double BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# ulab and sp version for PYBD_SF6 :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=single BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab, sp and thread version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# ulab, dp and thread version for PYBD_SF6 :
[stm32]$ make -j8 CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab, dp and thread version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# ulab, sp and thread version for PYBD_SF6 :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=single CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBD_SF6 USER_C_MODULES=../../../ulab all
# ulab, sp and network version, BOARD can be [PYBV11 PYBLITEV10] :
[stm32]$ make -j8 MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=PYBV11 USER_C_MODULES=../../../ulab all
# dp and network version, BOARD can be PYBV11 :                             
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=double MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=PYBV11 USER_C_MODULES=../../../ulab all
```
Loops for building firmwares with 'ulab', 'sp'/'dp' and with/without 'thread' for 5 Pyboard's :
```
# ulab and sp :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
# ulab and dp :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
# ulab, sp and thread :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
# ulab, dp and thread :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
```
Loop for building firmwares with 'ulab', 'sp' and 'network' for 2 Pyboard's :
```
[stm32]$ for BOARD in PYBV11 PYBLITEV10; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=$BOARD USER_C_MODULES=../../../ulab all; done
```
After each building loop, the 'firmware.dfu' placed at 'stm32/build-$BOARD' should be copied before the following loop.