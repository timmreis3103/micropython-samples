# MicroPython v1.12 firmwares for [Pyboard v1.1](https://store.micropython.org/product/PYBv1.1)/[Lite v1.0](https://store.micropython.org/product/PYBLITEv1.0)/[D SF2](https://store.micropython.org/product/PYBD-SF2-W4F2)/[D SF3](https://store.micropython.org/product/PYBD-SF3-W4F2)/[D SF6](https://store.micropython.org/product/PYBD-SF6-W4F2)

## Firmware features

The 24 MicroPython firmwares here are named in the form :  
```<Pyboard name>_<'sp_' or 'dp_'><optional 'thread_' or 'network_'><version>_<date>.dfu```  
where :

- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading;
- 'network' means Pyboard v1.1/Lite v1.0 firmware with drivers for [CC3000 WiFi modules](https://docs.micropython.org/en/latest/library/network.CC3K.html) and [WIZnet5x00 Ethernet modules](https://docs.micropython.org/en/latest/library/network.WIZNET5K.html).

For example :  
```pybd-sf2_dp_thread_v1.12-657-g37e1b5c89_2020-07-26.dfu```  
means it is a v1.12-657-g37e1b5c89, from July 26 2020, with double precision float point numbers and threads enabled, for Pyboard D SF2.


## (Optional) Building firmware with 'make' options

All MicroPython firmwares here were compiled using :
- the [MicroPython source code](https://github.com/micropython/micropython), v1.12-657-g37e1b5c89 from the commit [37e1b5c89...](https://github.com/micropython/micropython/commit/37e1b5c891f9964bb6c95228bc2d718511507a69), in July 24 2020;
- gcc-arm cross-compiler ['gcc-arm-none-eabi-8-2019-q3-update-linux.tar.bz2'](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads).

To build the MicroPython firmware for STM32 boards with 'ulab' module, look at the :  
- [instructions of MicroPython source code for STM32 boards](https://github.com/micropython/micropython/tree/master/ports/stm32);
- [Wiki 'Building Micropython Binaries'](https://github.com/micropython/micropython/wiki/Building-Micropython-Binaries).

Some of the commands that can be used to build the firmwares listed here, where BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6]. After building, the 'firmware.dfu' is placed at 'stm32/build-$BOARD', which should be copied before the following build.
```
# sp version is the default for [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 BOARD=PYBV11
# dp version is the default for PYBD_SF6 :                             
[stm32]$ make -j8 BOARD=PYBD_SF6
# dp version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=double BOARD=PYBV11
# sp version for PYBD_SF6 :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=single BOARD=PYBD_SF6
# sp and thread version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBV11
# dp and thread version for PYBD_SF6 :
[stm32]$ make -j8 CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBD_SF6
# dp and thread version, BOARD can be [PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3] :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBV11
# sp and thread version for PYBD_SF6 :
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=single CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=PYBD_SF6
# sp and network version, BOARD can be [PYBV11 PYBLITEV10] :
[stm32]$ make -j8 MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=PYBV11
# dp and network version, BOARD can be [PYBV11 PYBLITEV10] :                             
[stm32]$ make -j8 MICROPY_FLOAT_IMPL=double MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=PYBV11
```
Loops for building firmwares with 'sp'/'dp' and with/without 'thread' for 5 Pyboard's :
```
# sp :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single  BOARD=$BOARD; done
# dp :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single  BOARD=$BOARD; done
# sp and thread :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=$BOARD; done
# dp and thread :
[stm32]$ for BOARD in PYBV11 PYBLITEV10 PYBD_SF2 PYBD_SF3 PYBD_SF6; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=double CFLAGS_EXTRA='-DMICROPY_PY_THREAD=1' BOARD=$BOARD; done
```
Loops for building firmwares with 'sp'/'dp' and 'network' for 2 Pyboard's :
```
# sp and network :
[stm32]$ for BOARD in PYBV11 PYBLITEV10; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=single MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=$BOARD; done
# dp and network :
[stm32]$ for BOARD in PYBV11 PYBLITEV10; do make BOARD=$BOARD clean && make -j8 MICROPY_FLOAT_IMPL=double MICROPY_PY_CC3K=1 MICROPY_PY_WIZNET5K=5500 BOARD=$BOARD; done
```
After each building loop, the 'firmware.dfu' placed at 'stm32/build-$BOARD' should be copied before the following loop.
