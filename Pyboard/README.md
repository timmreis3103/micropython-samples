# MicroPython on Pyboard's

Tutorials, examples, tests (benchmarks), firmwares with many options (double precision, ulab module, etc), etc, about MicroPython on Pyboard's and similar boards.

TO DO :
- Using Pyboard Lite;
- Using Pyboard v1.1;
- Using Pyboard D SF2;
- Using Pyboard D SF3;
- Using Pyboard D SF6;
- Using shells, editores e IDE's com Pyboard;
- benchmarks about :
    - speed of GPIO output toggle;
    - reading ADC;
    - writing to DAC;
    - etc;
- tests of power usage using USB or battery;
- tests of time to wake up from lightsleep and deepsleep;
- firmwares Pyboard with :
    - frozen modules;
    - etc.