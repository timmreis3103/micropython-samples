# Usando LoPy 4

##### Autor : Roberto Colistete Jr. (roberto.colistete arroba gmail.com)
##### Última atualização : 17/06/2020.

LoPy 4 é uma placa que foi lançada pela Pycom no final de 2017, com microcontrolador ESP32, memória PSRAM de 4 MB, 
memória flash de 8 MB, 4 tipos de conexão sem fio (WiFi, Bluetooth, LoRa, Sigfox), 
[certificação (CE, FCC, LoRaWAN e Sigfox)](https://docs.pycom.io/documents/certificates/), 
alta qualidade de construção, com baixíssimo consumo em modo deepsleep (19-20 uA).
[LoPy 4 na Pycom Store custa EUR 34.95](https://pycom.io/product/lopy4/).

<img src="https://pycom.io/wp-content/uploads/2018/08/lopy4NoHeadersSideN.png" alt="LoPy 4 sem pinos" width="400"/>

## 1. Documentação do LoPy 4 e MicroPython :

- [Documentação do Pycom MicroPython](https://docs.pycom.io/);
- [LoPy 4 datasheet, diagrama de pinagem e vários links](https://docs.pycom.io/datasheets/development/lopy4/);
- ['Getting Started' com LoPy 4.](https://docs.pycom.io/gettingstarted/connection/lopy4/).

**ATENÇÂO : sempre ter conectada a antena LoRa** (conector no lado direito do LED RGB) ao usar LoPy 4

<img src="https://docs.pycom.io/gitbook/assets/lopy4-pinout.png" alt="Descrição da pinagem do LoPy 4" width="1000"/>

Especificações do LoPy 4, vide também [site da Expressif sobre ESP32](https://www.espressif.com/en/products/socs/esp32/overview) :
- ESP32 (ESP32D0WDQ6 rev. 1) com 2x 32 bit Xtensa 32-bit LX6 @ 160/240 MHz, suporte a número de ponto flutuante com precisão simples;
- 520 KB SRAM e 4 MB PSRAM (via QSPI);
- 448 kB ROM e 8 MB de memória flash (via QSPI);
- WiFi 802.11b/g/n 150 MBps, Bluetooth v4.2 BR/EDR and BLE;
- [transceiver Semtech SX1276](https://www.semtech.com/products/wireless-rf/lora-transceivers/sx1276) para LoRa com bandas 433 MHz, 
470-510 MHz, 868 MHz e 915 MHz e para Sigfox RCZ1-4;
- 34 GPIO (24 pinos expostos no LoPy 4, mas 3 usados por LoRa/Sigfox, pino P2 é para LED RGB/boot, pino P12 é para antena WiFi/modos de boot);
- 18 canais de ADC de 12 bits, 2 canais de DAC de bits, 16 portas PWM;
- 2x I2C (400 kHz master/slave), 4x SPI (up to 80 MHz master/slave), 2x I2S;
- 4x timers (64 bits com prescalers de 16-bits), 13 canais DMA, 3x UARTs;
- RTC com cristal de 32,768 kHz e 16 kB SRAM (8 kB slow SRAM, 8 kB fast SRAM);
- antena interna para WiFi, conectores para antenas externas para LoRa/Sigfox e WiFi;
- RGB LED WS2812;
- botão de reset;
- dimensões sem pinos : 55 mm x 20 mm x 3.5 mm;
- peso sem pinos : 7 g.

LoPy 4 tem a mais em relação a LoPy 1 (lançado em 02/2016, com entregas em 10/2016) :
- [ESP32D0WDQ6 revision 1 (x revision 0)](https://www.espressif.com/sites/default/files/documentation/eco_and_workarounds_for_bugs_in_esp32_en.pdf);
- 4 MB de RAM (x 520 kB);
- 8 MB de memória flash (x 4 MB);
- [transceiver Semtech SX1276 (x SX1272)](https://www.semtech.com/products/wireless-rf/lora-transceivers);
- LoRa cobrindo também bandas de 433 MHz, 470-510 MHz e Sigfox com conector extra para antena externa LoRa nessa faixa.


Nos códigos-fonte de testes :
- digite tecla 'Tab' após '.' em final de linha para ver a lista de atributos do objeto;
- saídas/resultados estão indentados com 1 tabulação a mais.


## 2. Testes e exemplos com MicroPython :

- usando firmware Pycom MicroPython oficial 1.20.2.rc7 (de 04/05/2020, não foi anunciado oficialmente em 
[Pycom MicroPython releases](https://github.com/pycom/pycom-micropython-sigfox/releases)) :
```
$ screen /dev/ttyACM0 115200
Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; LoPy4 with ESP32
import gc
gc.collect()
gc.mem_free()
    2560704   # 2500.687 kB de RAM livre
```

- [módulo 'os'](https://docs.pycom.io/firmwareapi/micropython/uos/) para ver versão de MicroPython, tamanho do sistema 
de arquivos da memória flash, etc :
```
import os
os.
    __class__       __name__        remove          chdir
    dupterm         fsformat        getcwd          getfree
    ilistdir        listdir         mkdir           mkfat
    mount           rename          rmdir           stat
    statvfs         sync            umount          uname
    unlink          urandom
os.uname()
    (sysname='LoPy4', nodename='LoPy4', release='1.20.2.rc7', version='v1.11-6d01270 on 2020-05-04', machine='LoPy4 with ESP32', lorawan='1.0.2', sigfox='1.0.1', pybytes='1.4.0')
os.statvfs('/flash')
    (4096, 4096, 1024, 1012, 1012, 0, 0, 0, 0, 1022)   # 4MB as file system from total of 8 MB of flash memory
```

- [módulo 'machine'](https://docs.pycom.io/firmwareapi/pycom/machine/), frequência do ESP32 (160 MHz, não dá para alterar), etc :
```
import machine
machine.
    __class__       __name__        main            ADC
    BROWN_OUT_RESET                 CAN             DAC
    DEEPSLEEP_RESET                 HARD_RESET      I2C
    PIN_WAKE        PWM             PWRON_RESET     PWRON_WAKE
    Pin             RMT             RTC             RTC_WAKE
    SD              SOFT_RESET      SPI             Timer
    Touch           UART            ULP_WAKE        WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 WDT             WDT_RESET
    deepsleep       disable_irq     enable_irq      flash_encrypt
    freq            idle            info            mem16
    mem32           mem8            pin_sleep_wakeup
    remaining_sleep_time            reset           reset_cause
    rng             secure_boot     sleep           temperature
    unique_id       wake_reason
machine.freq()
    160000000
machine.info()
    ---------------------------------------------
    System memory info (in bytes)
    ---------------------------------------------
    MPTask stack water mark: 8288
    ServersTask stack water mark: 3732
    LoRaTask stack water mark: 3516
    SigfoxTask stack water mark: 2936
    TimerTask stack water mark: 2212
    IdleTask stack water mark: 608
    System free heap: 390296
    --------------------------------------------
machine.temperature()   # in F
    132
(_ - 32)/1.8            # in C
    55.55556
machine.unique_id()
    b'0\xae\xa4Nd\xd4'
import ubinascii
ubinascii.hexlify(machine.unique_id())   # = Device ID = WiFi MAC address
    b'30aea44e64d4'
```

- [módulo 'micropython'](https://docs.pycom.io/firmwareapi/micropython/micropython/) :
```
import micropython
micropython.
    __class__       __name__        const
    alloc_emergency_exception_buf   heap_lock       heap_unlock
    kbd_intr        mem_info        opt_level       qstr_info
    stack_use
micropython.mem_info()
    stack: 736 out of 11264
    GC: total: 2561344, used: 992, free: 2560352
    No. of 1-blocks: 30, 2-blocks: 7, max blk sz: 8, max free sz: 159779
```

- help :
```
help()
    Welcome to MicroPython!
    For online docs please visit http://docs.pycom.io

    Control commands:
    CTRL-A        -- on a blank line, enter raw REPL mode
    CTRL-B        -- on a blank line, enter normal REPL mode
    CTRL-C        -- interrupt a running program
    CTRL-E        -- on a blank line, enter paste mode
    CTRL-F        -- on a blank line, do a hard reset of the board and enter safe boot

    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (pybytes, etc) da Pycom :
```
help('modules')
    __main__          _pybytes_main     micropython       uerrno
    _boot             _pybytes_protocol network           uhashlib
    _coap             _pybytes_pymesh_config              os                uio
    _flash_control_OTA                  _terminal         pycom             ujson
    _main             _thread           queue             umachine
    _main_pybytes     _urequest         re                uos
    _mqtt             array             select            uqueue
    _mqtt_core        binascii          socket            ure
    _msg_handl        builtins          ssl               uselect
    _periodical_pin   cmath             struct            uselect
    _pybytes          crypto            sys               usocket
    _pybytes_ca       errno             time              ussl
    _pybytes_config   framebuf          ubinascii         ustruct
    _pybytes_config_reader              gc                ubinascii         utime
    _pybytes_connection                 hashlib           ucollections      utimeq
    _pybytes_constants                  json              ucrypto           uzlib
    _pybytes_debug    machine           uctypes
    Plus any modules on the filesystem
```

- [LED RGB](https://docs.pycom.io/firmwareapi/pycom/pycom/), vide [tutorial](https://docs.pycom.io/tutorials/all/rgbled/). 
"By default the heartbeat LED flashes in blue colour once every 4s to signal that the system is alive" :
```
import pycom
pycom.heartbeat(False)    # Desabilita a pulsação do LED RGB
pycom.heartbeat()         # Mostra o estado da configuração de pulsação do LED RGB
    False
pycom.rgbled(0x0000ff)    # Azul, brilho máximo
pycom.rgbled(0x000010)    # Azul, brilho fraco
pycom.rgbled(0x00ff00)    # Verde, brilho máximo
pycom.rgbled(0xff0000)    # Vermelho, brilho máximo
pycom.rgbled(0xffffff)    # Branco, brilho máximo
pycom.rgbled(0x000000)    # Desligado
pycom.heartbeat(True)     # Habilita a pulsação do LED RGB
pycom.heartbeat_on_boot(False)   # Desabilita a pulsação do LED RGB, efetivo só no próximo boot
```

A FAZER exemplos sobre :
- GPIO com benchmark de chaveamento de saída;
- I2C;
- ADC com benchmark;
- DAC com benchmark;
- RTC;
- NVRAM (nvs_*);
- etc.


## 3. Consumo de energia :

LoPy 4 v1.0 + Expansion Board v3.0 só com jumpers RX/TX (ou seja, sem RTS/CTS jumpers).  
Firmware : Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; LoPy4 with ESP32

### 3.1. Alimentação com cabo USB e terminal REPL aberto, medidor USB [UM25C](https://www.aliexpress.com/item/32855845265.html).

Tensão Vusb = 5,07-5,08 V.

O default após o firmware ser gravado é ter WiFi habilitado em modo AP (Access Point) :
* i = 119-141 mA com PyBytes off, WiFi on (AP on/STA off), BLE on;
* i = 108-109 mA com PyBytes off, WiFi on (AP off/STA on), BLE on;
* i = 47,5-48,0 mA com PyBytes off, WiFi off, BLE on;
* i = 19,0-19,4 mA em light sleep com PyBytes off, WiFi off, BLE on;
* i = 16,8-17,2 mA em deep sleep com PyBytes off, WiFi off, BLE on;
* LED RBG com 'pycom.heartbeat(False)' e PyBytes off, WiFi off, BLE on :
    + i = 47,5-48,0 mA com LED RBG apagado;
    + i = 81,0-81,5 mA com LED RBG branco, brilho máximo muito forte, "pycom.rgbled(0xffffff)", aumento de 33,0-34,0 mA;
    + i = 61,6-62,1 mA com LED RBG vermelho, brilho máximo muito forte, "pycom.rgbled(0xff0000)", aumento de 13,6-14,6 mA;
    + i = 57,8-58,1 mA com LED RBG verde, brilho máximo muito forte, "pycom.rgbled(0x00ff00)", aumento de 9,8-10,6 mA;
    + i = 59,2-59,7 mA com LED RBG azul, brilho máximo muito forte, "pycom.rgbled(0x0000ff)", aumento de 11.2-12,2 mA;
    + i = 63,3-63,7 mA com LED RBG branco, brilho forte, "pycom.rgbled(0x808080)", aumento de 15,3-16,2 mA;
    + i = 54,7-55,1 mA com LED RBG branco, brilho médio, "pycom.rgbled(0x404040)", aumento de 6,7-7,6 mA;
    + i = 48,1-48,6 mA com LED RBG branco, brilho fraco, "pycom.rgbled(0x101010)", aumento de 0,1-1,1 mA;
    + i = 47,7-48,1 mA com LED RBG branco, brilho muito fraco, "pycom.rgbled(0x080808)", aumento de 0,1-0,6 mA;

- [desabilitar heart beat RGB LED](https://docs.pycom.io/firmwareapi/pycom/pycom/), assim i oscila menos (somente na faixa de 0,2 mA) :
```
import pycom
pycom.heartbeat(False)
# ou :
pycom.heartbeat_on_boot(False)       # efetivo só no próximo boot
```

- [desabilitar PyBytes](https://forum.pycom.io/topic/5649/new-firmware-release-1-20-2-rc3) :
```
import pycom
pycom.pybytes_on_boot(False)         # efetivo só no próximo boot, normalmente já vem com False após gravar firmware sem habilitar PyBytes
```

- [desabilitar WiFi](https://development.pycom.io/firmwareapi/pycom/pycom/) :
```
import network
wlan = network.WLAN(mode=network.WLAN.STA)
wlan.mode()   # 1 = wlan.STA
wlan.deinit()
#
import network
wlan = network.WLAN(mode=network.WLAN.AP, ssid='LoPy4')
wlan.mode()   # 2 = wlan.AP
# ou  :
import pycom
pycom.wifi_on_boot(False)            # efetivo só no próximo boot, normalmente já vem com True após gravar firmware
```

- [desabilitar BLE - Bluetooth Low Energy](https://docs.pycom.io/firmwareapi/pycom/network/bluetooth/), mas i varia fração de mA, 
difícil de medir (inclusive parece que aumenta ao rodar o 'ble.deinit()':
```
import network
ble = network.Bluetooth()
ble.deinit()
```

- [light sleep](https://development.pycom.io/firmwareapi/pycom/machine/), modo de economia de energia leve em que [a CPU é 
pausada e periféricos desligados (inclusive WiFi e Bluetooth)](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-light-sleep), 
continuando de onde parou após terminar :
```
import machine
machine.sleep(60000)   # 60000 ms = 60,000 s
```

- [deep sleep](https://development.pycom.io/firmwareapi/pycom/machine/), modo de economia de energia que [desliga tudo menos 
o RTC](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-deep-sleep), dando reboot após terminar :
```
import machine
machine.deepsleep(60000)   # 60000 ms = 60,000 s
```


### 3.2. Alimentação via bateria, medindo via multímetro
