# DAC_loop_Pycom_Chrono.py
# Outputs to the xxPy (LoPy/LoPy4/etc) DAC and measures the time (using 'Timer.Chrono()') and speed.
import machine

def DACloopBenchmark(nsamples=1000):
    dac = machine.DAC('P22')
    chrono = machine.Timer.Chrono()
    chrono.start()
    for i in range(nsamples):
        dac.write(i/nsamples)
    dt = chrono.read_us()
    print("%u DAC outputs done after %5.3f ms" % (nsamples, dt/1e3))
    print("Mean time for each DAC output = %4.2f us" % (dt/nsamples))
    print("DAC speed = %5.3f ksamples/s" % (1e3*nsamples/dt))

